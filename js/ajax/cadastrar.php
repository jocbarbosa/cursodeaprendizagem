<?php

unset($_POST['email2']);

// ini_set('display_errors', 1);
// error_reporting(E_ALL);

include("../../classes/config.php");
include("../../classes/DB.class.php");
include("../../classes/CRUD.class.php");
include("../../classes/Geral.class.php");
include('../../classes/PHPMailerAutoload.php');

$resposta = array(
    "status" => "procesando",
    "mensagem" => "<p>Processo não concluído!</p>",
);
$mensagem = "";

extract($_POST);

$email = mb_strtolower($email);
$email = trim($email);
$_POST['email'] = $email;

$nome = mb_strtoupper($nome);
$nome = trim($nome);
$_POST['nome'] = $nome;

$_POST['senha_decrip'] = $_POST['senha'];

if (empty($nome)) {
    $mensagem .= "<p>É importante informar nome e nome.</p>";
}

$sobrenome = explode(' ', $nome);
if (count($sobrenome) == 1) {
    $mensagem .= "<p>É importante informar nome e sobrenome.</p>";
}

if (empty($email)) {
    $mensagem .= "<p>E-mail é um campo necessário.</p>";
}

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $mensagem .= "<p>Verifique o campo e-mail.</p>";
} else {
    $inscrito = CRUD::SelectOne('inscricoes', 'email', $email);
    if ($inscrito['num'] > 0) {
        $mensagem .= "<p>Este e-mail já está incrito no curso.</p>";
    }
}

if (strlen($senha) < 6) {
    $mensagem .= "<p>A senha deve ter no mínimo 6 caracteres.</p>";
}

$telefone = preg_replace('/[^0-9]/', '', $telefone);
if (strlen($telefone) < 10) {
    $mensagem .= "<p>Por favor informe o telefone.</p>";
}

if (empty($estado)) {
    $mensagem .= "<p>Por favor informe o estado.</p>";
}

if (empty($cidade)) {
    $mensagem .= "<p>Por favor informe a cidade.</p>";
}

if (empty($escola)) {
    $mensagem .= "<p>Selecione sua escola.</p>";
}

if (empty($perfil)) {
    $mensagem .= "<p>Informe se você é: Educador, Família, Aluno, Gestão Pedagógica ou Administrativo.</p>";
}

if ($mensagem == "") {
    
    $id_inscricao = CRUD::Insert('inscricoes');
    
    if (time() >= timesdata($data_inicio) && $id_inscricao) {        
        $botao = 'ACESSE O CURSO AQUI!';
    } else {
        $botao = 'ACESSE O CURSO EM '.$data_inicio;
    }
    
    if ($id_inscricao) {
        $mensagem = file_get_contents("email.html");
        $mensagem = str_replace('[URL]', URLBASE, $mensagem);
        $mensagem = str_replace('[NOME]', $nome, $mensagem);
        $mensagem = str_replace('[USUARIO]', $email, $mensagem);
        $mensagem = str_replace('[SENHA]', $senha, $mensagem);
        $mensagem = str_replace('[BOTAO]', $botao, $mensagem);
        $mensagem = str_replace('[INICIO]', $data_inicio, $mensagem);
        $mensagem = str_replace('[TERMINO]', $data_termino, $mensagem);
        $altbody = 'Inscrição';
        // $envia = Geral::SendMail($nome . ', inscrição confirmada!', $altbody, $mensagem, $email, $nome);
    }

} 

//echo '<pre>'; print_r($mensagem); echo '</pre>';
//echo '<pre>'; print_r($_POST); echo '</pre>';
//die();

$resposta = array();

if (isset($id_inscricao) && !empty($id_inscricao)) {
    header('Content-Type: application/json');
    $resposta['status'] = 'Ok';
    $resposta['mensagem'] = "
        <h3>$nome, sua inscrição foi realizada com sucesso!</h3>
        <p class='ok'>
            Que bom ter você nesta importante jornada!
            A sua inscrição para o Curso  Avaliação da Aprendizagem Escolar já está confirmada.
            Depois de <strong class='dias-uteis-texto'>DOIS DIAS ÚTEIS</strong> você receberá um e-mail e poderá realizar o seu primeiro acesso ao curso.
        </p>
        <div class='texto-destaque-resposta'>
            <h4 class='atencao-texto'>ATENÇÃO</h4>
            <p>
                Para acessar o curso, preencha os campos \"usuário\" e \"senha\" com o e-mail cadastrado na inscrição.
            </p>
        </div>
    ";
    echo json_encode($resposta);
} else {
    $resposta['status'] = 'Erro';
    if (!empty($mensagem)) {
        $resposta['mensagem'] = "<h3>Desculpe!</h3> $mensagem";
    } else {
        $resposta['mensagem'] = "
            <h3>Desculpe!</h3>
            <p>Não foi possível realizar o seu cadastro.</p>
        ";
    }
    header('Content-Type: application/json');
    echo json_encode($resposta);    
}
?>
 
 