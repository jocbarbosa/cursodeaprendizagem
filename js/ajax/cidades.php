<?php include("../../classes/config.php"); ?>
<?php include("../../classes/DB.class.php"); ?>
<?php include("../../classes/CRUD.class.php"); ?>
<?php include('../../classes/PHPMailerAutoload.php'); ?>

<?php
    echo '<option value=""></option>';
    extract($_POST);
    $sel = CRUD::SelectOne('cidades','estado',$estado,'cidade ASC');
    foreach ($sel['dados'] as $cidades) {
        if (isset($id) && !empty($id) && $id == $cidades['id']) {
            $selecionado = 'selected="selected"';
        } else {
            $selecionado = '';
        }
        echo '<option value="'.$cidades['id'].'"'.$selecionado.'>'.$cidades['cidade'].'</option>';
    }
?>