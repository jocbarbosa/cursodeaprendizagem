        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header text-center">
                    <a class="navbar-brand" href="index.php">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="assets/images/sas-logo.png" alt="homepage" class="dark-logo" style="height: 30px">
                            <!-- Light Logo icon -->
                            <img src="assets/images/logo-icon.png" alt="homepage" class="light-logo" style="height: 30px">
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item"> <a class="nav-link nav-toggler d-block d-md-none waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark" href="javascript:void(0)"><i class="icon-menu"></i></a> </li>
                    </ul>
                    <?php 
                    $usuriologado = CRUD::SelectOne('administradores', 'id', $_SESSION[SESSION_ADMIN . '_uid']);
                    $usuriologado = $usuriologado['dados'][0];
                    
                    if (file_exists('images/usuarios/'.$usuriologado['foto'])) {
                        $avatar = 'images/usuarios/'.$usuriologado['foto'];
                    } else {
                        $avatar = 'images/avatar.png';
                    }
                    ?>
                    <ul class="navbar-nav my-lg-0">                     
                        <li class="nav-item">
                            <h5 class="user-name">Painel de Inscrições<br/><small>Usuário: <?php echo $usuriologado['nome'] ?></small></h5>
                        </li>
                    </ul>
                </div>
            </nav>
		</header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->