<?php
include('templates/header.php');

extract($_GET);
$sel = array();
if (isset($id)) {
    $sel = CRUD::SelectOne('administradores', 'id', $id);
}

if (isset($_POST['salvar'])) {
    $sel['dados'][] = $_POST;
}

//echo '<pre>'; print_r($_POST); echo '</pre>';
?>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div id="main-wrapper">
    <?php include('templates/topo.php'); ?>
    <?php include('templates/menu.php'); ?>
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Administradores</h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Administradores</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Usuário Administrador</h4>
                            <!-- <h6 class="card-subtitle">Os QRCode são utilizados para trasmitir informações necesárias de forma segura. Este QR Code dará a capacidade de um ou mais alunos se cadastram nas aplicações selecionadas. </h6> -->
                            <form class="m-t-40" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <h5>Nome <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="nome" value="<?php echo isset($sel['dados'][0]['nome']) ? $sel['dados'][0]['nome'] : '' ?>" class="form-control" required data-validation-required-message="Este Campo é Obrigatório">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>E-mail</h5>
                                    <div class="controls">
                                        <input type="email" name="email" value="<?php echo isset($sel['dados'][0]['email']) ? $sel['dados'][0]['email'] : '' ?>" class="form-control" required data-validation-required-message="Este Campo é Obrigatório">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Login <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="login" value="<?php echo isset($sel['dados'][0]['login']) ? $sel['dados'][0]['login'] : '' ?>" class="form-control" required data-validation-required-message="Este Campo é Obrigatório">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Senha <span class="text-danger">*</span></h5>
                                    <div class="controls local-senha">
                                        <input type="password" id="senha" name="senha" class="form-control" <?php echo !isset($id) || empty($id) ?  'required data-validation-required-message="Este Campo é Obrigatório"' : '' ?> >
                                        <i class="fa fa-eye ver-senha texto-formulario"></i>
                                    </div>
                                </div>
                                <div class="text-xs-right">
                                    <button type="submit" name="salvar" class="btn btn-info">Salvar</button>
                                    <a href="<?php echo $url_site_admin . 'lista_usuarios.php' ?>" class="btn btn-inverse btn-danger">Cancelar</a>
                                </div>
                                <?php
                                $erros = '';
                                if (isset($_POST['salvar'])) {

                                    $login_cadastrado = CRUD::SelectOne('administradores', 'login', $_POST['login']);

                                    if ($login_cadastrado['num'] > 0 && $login_cadastrado['dados'][0]['id'] <> $_GET['id']) {
                                        $erros = 'login em uso';
                                        ?>
                                        <div id="modal_erros" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel">Login em uso!</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Já existe um usuário cadastrado com este login, altere o login e tente novamente.</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    } else {
                                        $salvar = CRUD::Insert('administradores', $_GET['id'], 'images/usuarios/', 'no');
                                        header('Location: ' . $url_site_admin . 'lista_usuarios.php');
                                    }
                                }
                                ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <?php include('templates/sidebar.php'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
    <?php include('templates/footer.php'); ?>

    <script>
        <?php if (isset($erros) && !empty($erros)) { ?>
            $('#modal_erros').modal();
        <?php } ?>
    </script>
    
    <style>
            form .local-senha {
                position: relative !important;
            }
            form .ver-senha {
                /*opacity: 0.4;*/
                color: #6c757d!important;
                font-size: 18px !important;
                cursor: pointer !important;
                position: absolute !important;
                top: 10px !important;
                right: 15px !important;
                z-index: 99 !important;
            }
        </style>
        <script type="text/javascript">
            $(document).on('click','.ver-senha', function (e) {
                if ($(this).hasClass('fa-eye')) {
                    $(this).removeClass('fa-eye')
                    $(this).addClass('fa-eye-slash')
                    $(this).siblings('#senha').attr('type', 'text');
                    $(this).siblings('#senha').focus();
                } else {
                    $(this).removeClass('fa-eye-slash')
                    $(this).addClass('fa-eye')
                    $(this).siblings('#senha').attr('type', 'password');
                    $(this).siblings('#senha').focus();
                }     
            });  
        </script>