<?php
/*
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL ^E_NOTICE);
*/

$server = $_SERVER['SERVER_NAME'];

if ($server == 'localhost' || $server == '192.168.15.11') {

    $url_site = 'http://'.$server.':/inscricao/';
    $url_site_admin = 'http://'.$server.':/inscricao/admin/';

    define('DB_HOST', 'localhost');
    define('DB_NAME', 'avaliacaoaprendizagem');
    define('DB_USER', 'avaliacaoaprendizagem');
    define('DB_PASS', 'Ris16Lin81!!');

} else {

    $url_site = 'https://focos.saseducacao.com.br/cursodeaprendizagem/';
    $url_site_admin = 'https://focos.saseducacao.com.br/cursodeaprendizagem/admin-sasprd';

    define('DB_HOST', 'localhost');
    define('DB_NAME', 'avaliacaoaprendizagem');
    define('DB_USER', 'avaliacaoaprendizagem');
    define('DB_PASS', 'Ris16Lin81!!');

} 

// ENVIO DE E-MAIL
define('MAIL_HOST', '');
define('MAIL_USER', '');
define('MAIL_PASS', '');
define('MAIL_PORT', '');
define('MAIL_SECURE', '');
define('MAIL_SEND', '');
define('MAIL_NAME', '');

// URL's
define('URLBASE', $url_site);

// SITE INFO
define('SITE_NAME', 'Incrições');
define('SESSION', 'INSC');
define('SESSION_ADMIN', 'INSC_ADMIN');
define('REGULAMENTO', 'caminho_do_arquivo');

$estadosBrasileiros = array(
    'AC' => 'Acre',
    'AL' => 'Alagoas',
    'AP' => 'Amapá',
    'AM' => 'Amazonas',
    'BA' => 'Bahia',
    'CE' => 'Ceará',
    'DF' => 'Distrito Federal',
    'ES' => 'Espírito Santo',
    'GO' => 'Goiás',
    'MA' => 'Maranhão',
    'MT' => 'Mato Grosso',
    'MS' => 'Mato Grosso do Sul',
    'MG' => 'Minas Gerais',
    'PA' => 'Pará',
    'PB' => 'Paraíba',
    'PR' => 'Paraná',
    'PE' => 'Pernambuco',
    'PI' => 'Piauí',
    'RJ' => 'Rio de Janeiro',
    'RN' => 'Rio Grande do Norte',
    'RS' => 'Rio Grande do Sul',
    'RO' => 'Rondônia',
    'RR' => 'Roraima',
    'SC' => 'Santa Catarina',
    'SP' => 'São Paulo',
    'SE' => 'Sergipe',
    'TO' => 'Tocantins'
);

$data_inicio = '02/03/2021';
$data_termino = '23/05/2021';
$data_incricao = '02/03/2021';
$data_termino_inscricao='22/03/2021';

function timesdata($data) {
    $partes = explode('/', $data);
    $dia = (int)$partes[0];
    $mes = (int)$partes[1];
    $ano = (int)$partes[2];
    return strtotime($dia."-".$mes."-".$ano);
}
