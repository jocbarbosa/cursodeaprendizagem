<?php
include('templates/header.php');
extract($_GET);
$sel = array();

$erros = '';
if (isset($_POST['salvar'])) {
    
    if (isset($_POST) && !empty($_POST)) {
        extract($_POST);

        if (!empty($_POST['nivel'])) {
            $_POST['nivel'] = implode(' / ', $_POST['nivel']);
        }

        //echo '<pre>'; print_r($_POST); echo '</pre>';

        $request = md5(implode($_POST));

        if (isset($_SESSION['last_request']) && $_SESSION['last_request'] == $request && $ignorar != true) {

            header('Location: index.php');

        } else {

            $_SESSION['last_request'] = $request;
            $nome = formatar_nome($nome);
            $_POST['nome'] = $nome;  
            $_POST['email'] = trim(strtolower($_POST['email']));         
            $_POST['alterado'] = trim(strtolower($_SESSION['CC_ADMIN_uid']));         
            
            $ver = CRUD::SelectExtra("SELECT * FROM inscricao WHERE email = '{$_POST['email']}' AND id <> {$_GET['id']}");

            if ($ver['num'] == 0 || $ignorar == true) {

                unset($_POST['nivel']);
                unset($_POST['nivel_aluno']);

                switch ($perfil_id) {
                    case 5: //Administrativo
                        unset($_POST['cargo_id']);
                        break;
                    case 4: //Pedagogico
                        unset($_POST['departamento_id']);
                        break;
                    case 3: //Aluno
                    case 2: //Familia
                    case 1: //Educador     
                        unset($_POST['departamento_id']);
                        unset($_POST['cargo_id']);
                        break;
                }

                $id = CRUD::Insert('inscricao', $_GET['id']);                        

                if ($id != 0) {

                    switch ($perfil_id) {
                        case 3: //Aluno
                            CRUD::DeleteExtra("DELETE FROM inscricao_nivel WHERE inscricao_id = {$_GET['id']}");
                            $_POST = array('inscricao_id' =>$_GET['id'], 'nivel_id' => $nivel_aluno);
                            CRUD::Insert('inscricao_nivel');
                            break;
                        case 2: //Familia
                        case 1: //Educador
                            CRUD::DeleteExtra("DELETE FROM inscricao_nivel WHERE inscricao_id = {$_GET['id']}");
                            if (!empty($nivel)) {
                                foreach ($nivel as $nivel_id) {
                                    $_POST = array('inscricao_id' =>$_GET['id'], 'nivel_id' => $nivel_id);
                                    CRUD::Insert('inscricao_nivel');
                                }
                            }
                            break;
                    }

                    $mensagem_modal =  "<h3>Alterado com sucesso.</h3>";
                    
                } else {
                    $mensagem_modal = '<h3>Falha ao Salvar!</h3>';
                }
            } else {
                $mensagem_modal = '<h3>Já existe uma inscrição com este email!</h3>';
            }                
        }            
    }

    if (!empty($mensagem_modal)) {
        ?>
        <div id="modal_ok" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Sucesso!</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <p><?php echo $mensagem_modal ?></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}

if (isset($id)) {
    $sel = CRUD::SelectOne('inscricao', 'id', $id);
    $cadastro = $sel['dados'][0];
}

$cadastro_niveis = array();
$sel = CRUD::SelectOne('inscricao_nivel', 'inscricao_id', $id);
if ($sel['num'] > 0) {
    foreach ($sel['dados'] as $dados) {
        $cadastro_niveis[] = $dados['nivel_id'];
    }
}
?>
<style>
    .extra_nao {
        display: none;
    }
</style>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div id="main-wrapper">
    <?php include('templates/topo.php'); ?>
    <?php include('templates/menu.php'); ?>
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Cadastro</h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Cadastro</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title">Cadastro do Participante</h4>

                            <form class="m-t-40" novalidate method="post" enctype="multipart/form-data">

                                <?php //echo '<pre>'; print_r($_SESSION); echo '</pre>'; ?>
                                <?php //echo '<pre>'; print_r($cadastro_niveis); echo '</pre>'; ?>
                                <?php //echo '<pre>'; print_r($cadastro_niveis); echo '</pre>'; ?>
                                
                                <input type="hidden" name="id" value="<?php echo $cadastro['id'] ?>">
                                
                                <div class="form-group">
                                    <h5>Nome <span class="text-danger">*</span></h5>
                                    <input type="text" class="form-control" id="nome" name="nome" aria-describedby="emailHelp" placeholder="Nome completo" value="<?php echo $cadastro['nome'] ?>" required>
                                </div>

                                <div class="form-group">
                                    <h5>Email <span class="text-danger">*</span></h5>
                                    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Email" value="<?php echo $cadastro['email'] ?>" required>
                                </div>

                                <div class="form-group">
                                    <h5>Telefone <span class="text-danger">*</span></h5>
                                    <input type="tel" class="form-control" name="telefone" aria-describedby="emailHelp" placeholder="Telefone com DDD" minlength="10" value="<?php echo $cadastro['telefone'] ?>" required>
                                </div>

                                <div class="form-group">
                                    <?php $perfis = CRUD::Select('perfil', 'ordem'); ?>
                                    <h5>Perfil <span class="text-danger">*</span></h5>
                                    <select class="form-control" id="perfil_id" name="perfil_id" required>
                                        <?php foreach ($perfis['dados'] as $registro) { ?>
                                            <?php 
                                            if ($registro['id'] == $cadastro['perfil_id']) {
                                                $selecionado = 'selected="selected"';
                                            } else {
                                                $selecionado = '';
                                            }
                                            ?>
                                            <option value="<?php echo $registro['id'] ?>" data-apelido="<?php echo $registro['apelido'] ?>" <?php echo $selecionado ?>><?php echo $registro['nome'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>  

                                <div>
                                    <div class="form-group extra extra_unidade">
                                        <?php $unidades = CRUD::Select('unidade', 'ordem'); ?>
                                        <h5>Unidade <span class="text-danger">*</span></h5>
                                        <select class="form-control" id="unidade_id" name="unidade_id">                                            
                                            <?php foreach ($unidades['dados'] as $registro) { ?>
                                                <?php 
                                                if ($registro['id'] == $cadastro['unidade_id']) {
                                                    $selecionado = 'selected="selected"';
                                                } else {
                                                    $selecionado = '';
                                                }
                                                ?>
                                                <option value="<?php echo $registro['id'] ?>" <?php echo $selecionado ?>><?php echo $registro['nome'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group extra extra_departamento <?php echo in_array($cadastro['perfil_id'], [5]) ? '' : 'extra_nao' ?>">
                                        <?php $departamentos = CRUD::Select('departamento', 'ordem'); ?>
                                        <h5>Departamento <span class="text-danger">*</span></h5>
                                        <select class="form-control" id="departamento_id" name="departamento_id">
                                            <option value="">Selecione</option>
                                            <?php foreach ($departamentos['dados'] as $registro) { ?>
                                                <?php 
                                                if ($registro['id'] == $cadastro['departamento_id']) {
                                                    $selecionado = 'selected="selected"';
                                                } else {
                                                    $selecionado = '';
                                                }
                                                ?>
                                                <option value="<?php echo $registro['id'] ?>" <?php echo $selecionado ?>><?php echo $registro['nome'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group extra extra_cargo <?php echo in_array($cadastro['perfil_id'], [4]) ? '' : 'extra_nao' ?>">
                                        <?php $cargos = CRUD::Select('cargo', 'ordem'); ?>
                                        <h5>Cargo <span class="text-danger">*</span></h5>
                                        <select class="form-control" id="cargo_id" name="cargo_id">
                                            <option value="">Selecione</option>
                                            <?php foreach ($cargos['dados'] as $registro) { ?>
                                                <?php 
                                                if ($registro['id'] == $cadastro['cargo_id']) {
                                                    $selecionado = 'selected="selected"';
                                                } else {
                                                    $selecionado = '';
                                                }
                                                ?>
                                                <option value="<?php echo $registro['id'] ?>" <?php echo $selecionado ?>><?php echo $registro['nome'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group extra extra_nivel_aluno <?php echo in_array($cadastro['perfil_id'], [3]) ? '' : 'extra_nao' ?>">
                                        <?php $niveis = CRUD::Select('nivel', 'ordem'); ?>
                                        <h5>Nível escolar <span class="text-danger">*</span></h5>
                                        <select class="form-control" id="nivel_aluno" name="nivel_aluno">
                                            <option value="">Selecione</option>
                                            <?php foreach ($niveis['dados'] as $registro) { ?>
                                                <?php 
                                                if ($registro['id'] == $cadastro['nivel_id'] && $registro['perfil_id'] == 3) {
                                                    $selecionado = 'selected="selected"';
                                                } else {
                                                    $selecionado = '';
                                                }
                                                ?>
                                                <option value="<?php echo $registro['id'] ?>" <?php echo $selecionado ?>><?php echo $registro['nome'] ?></option>
                                            <?php } ?>                                          
                                        </select>
                                    </div>

                                    <div class="nivel_ensino extra extra_nivel">
                                        <h5 class="pb-1">Níveis escolares <span class="text-danger">*</span></h5>
                                        <div class="row">
                                            <?php $niveis = CRUD::SelectOne('nivel', 'ordem', '1', 'id'); ?>                                 
                                            <div class="col-md-4">
                                                <?php foreach ($niveis['dados'] as $registro) { ?>
                                                    <div class="form-check">
                                                        <?php 
                                                        if (in_array($registro['id'], $cadastro_niveis) && in_array($cadastro['perfil_id'], [1, 2])) {
                                                            $selecionado = 'checked="checked"';
                                                        } else {
                                                            $selecionado = '';
                                                        }
                                                        ?>
                                                        <input class="form-check-input" id="nivel_<?php echo $registro['id'] ?>" name="nivel[]" type="checkbox" value="<?php echo $registro['id'] ?>" <?php echo $selecionado ?>>
                                                        <label class="form-check-label" for="nivel_<?php echo $registro['id'] ?>"><?php echo $registro['nome'] ?></label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <?php $niveis = CRUD::SelectOne('nivel', 'ordem', '2', 'id'); ?>                                 
                                            <div class="col-md-4">
                                                <?php foreach ($niveis['dados'] as $registro) { ?>
                                                    <div class="form-check">
                                                        <?php 
                                                        if (in_array($registro['id'], $cadastro_niveis) && in_array($cadastro['perfil_id'], [1, 2])) {
                                                            $selecionado = 'checked="checked"';
                                                        } else {
                                                            $selecionado = '';
                                                        }
                                                        ?>
                                                        <input class="form-check-input" id="nivel_<?php echo $registro['id'] ?>" name="nivel[]" type="checkbox" value="<?php echo $registro['id'] ?>" <?php echo $selecionado ?>>
                                                        <label class="form-check-label" for="nivel_<?php echo $registro['id'] ?>"><?php echo $registro['nome'] ?></label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <?php $niveis = CRUD::SelectOne('nivel', 'ordem', '3', 'id'); ?>                                 
                                            <div class="col-md-4">
                                                <?php foreach ($niveis['dados'] as $registro) { ?>
                                                    <div class="form-check">
                                                        <?php 
                                                        if (in_array($registro['id'], $cadastro_niveis) && in_array($cadastro['perfil_id'], [1, 2])) {
                                                            $selecionado = 'checked="checked"';
                                                        } else {
                                                            $selecionado = '';
                                                        }
                                                        ?>
                                                        <input class="form-check-input" id="nivel_<?php echo $registro['id'] ?>" name="nivel[]" type="checkbox" value="<?php echo $registro['id'] ?>" <?php echo $selecionado ?>>
                                                        <label class="form-check-label" for="nivel_<?php echo $registro['id'] ?>"><?php echo $registro['nome'] ?></label>
                                                    </div>
                                                <?php } ?>
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="text-xs-right">
                                    <button type="submit" name="salvar" class="btn btn-info">Salvar</button>
                                    <a href="<?php echo $url_site_admin . 'procurar_alunos.php' ?>" class="btn btn-inverse btn-danger">Cancelar</a>
                                </div>                                    
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <?php include('templates/sidebar.php'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
    <?php include('templates/footer.php'); ?>

    <script>
        <?php if (isset($mensagem_modal) && !empty($mensagem_modal)) { ?>
            $('#modal_ok').modal();
        <?php } ?>
    </script>
    
    <script src="../js/main.js"></script>