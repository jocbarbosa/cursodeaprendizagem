<?php

class CRUD extends DB {

    private static function CripSenha($senha) {
        $senha = openssl_digest($senha, 'sha512');
        return $senha;
    }

    private static function Upload($nome_img, $pasta, $renomear) {
        $imagem = Geral::salva($pasta, $nome_img, $renomear);
        return $imagem;
    }

    private static function UploadMultiplo($valor_file, $pasta, $tabela, $nome_file, $id) {
        $imagem = Geral::SalvaMultiplo($valor_file, $pasta, $tabela, $nome_file, $id);
        return $imagem;
    }

    private static function FormataCampos($pasta, $renomear, $tabela, $id) {
        $values = array();
        $img = array();
        foreach ($_POST as $nome_campo => $valor) {
            if (($nome_campo != 'salvar') && ($nome_campo != 'csenha') && ($nome_campo != 'salva_news')) {
                if (is_array($valor))
                    $valor = implode(',', $valor);
                $values = $values + array($nome_campo => $valor);
            }
        }
        if (isset($_FILES)) {
            foreach ($_FILES as $nome_file => $valor_file) {
                if (!is_array($valor_file['name'])) {
                    $up_img = self::Upload($nome_file, $pasta, $renomear);
                    foreach ($valor_file as $key => $value) {
                        if (($key == 'name') && ($up_img != '')) {
                            if ($id != 0) {
                                $del_img = self::DeleteImagem($tabela, $pasta, $id);
                            }
                            $img = $img + array($nome_file => $up_img);
                        }
                    }
                }
            }
            $values = $values + $img;
        }
        return $values;
    }

    public static function Insert($tabela, $id = 0, $pasta = '', $renomear = '') {

        $formata_dados = '';

        $dados = self::FormataCampos($pasta, $renomear, $tabela, $id);

        foreach ($dados as $key => $value) {
            if ($key == 'senha') {
                if ($value != '') {
                    $value = self::CripSenha($value);
                    $formata_dados = $formata_dados . $key . " = '" . $value . "', ";
                }
            } else if ($key == 'url') {
                if ($value != '') {
                    $value = Geral::urlamigavel($value);
                    $formata_dados = $formata_dados . $key . " = '" . $value . "', ";
                }
            } else
                $formata_dados = $formata_dados . $key . " = '" . $value . "', ";
        }
        $formata_dados = substr($formata_dados, 0, -2);

        if ($id == 0) {

            //echo "INSERT INTO {$tabela} SET {$formata_dados}";

            $insert = self::getConn()->prepare("INSERT INTO {$tabela} SET {$formata_dados}");
            $insert->execute();
            $lastId = self::getConn()->lastInsertId();
        } else {
            $update = self::getConn()->prepare("UPDATE {$tabela} SET {$formata_dados} WHERE `id`=?");
            $update->execute(array($id));
            $lastId = $id;
        }

        if (isset($_FILES['arquivos'])) {
            $up_img = self::UploadMultiplo($_FILES['arquivos'], $pasta . '/pdf', $tabela . '_arquivos', 'id_' . $tabela, $lastId);
        }


        return $lastId;
    }

    static function SelectAnexo($tabela, $campo, $id) {
        $list = self::getConn()->prepare("SELECT * FROM {$tabela} WHERE {$campo} = ? ORDER BY `id` DESC");
        $list->execute(array($id));
        $d['num'] = $list->rowCount();
        $d['dados'] = $list->fetchAll();
        return $d;
    }

    public static function InsertAjax($tabela, $dados) {
        $insert = self::getConn()->prepare("INSERT INTO {$tabela} SET {$dados}");
        $insert->execute();
    }

    public static function Select($tabela, $ordem = 'id DESC') {
        $select = self::getConn()->prepare("SELECT * FROM {$tabela} ORDER BY {$ordem}");
        $select->execute();

        $d['num'] = $select->rowCount();
        $d['dados'] = $select->fetchAll();

        return $d;
    }

    public static function SelectGroup($tabela, $group, $ordem = 'id DESC') {
        $select = self::getConn()->prepare("SELECT * FROM {$tabela} GROUP BY {$group} ORDER BY {$ordem}");
        $select->execute();

        $d['num'] = $select->rowCount();
        $d['dados'] = $select->fetchAll();

        return $d;
    }

    public static function Select2($tabela1, $tabela2, $id) {
        $field = 'id_' . $tabela1;
        $select = self::getConn()->prepare("SELECT * FROM {$tabela1} INNER JOIN {$tabela2} ON {$tabela1}.`id` = {$tabela2}.{$field} WHERE {$tabela1}.`id` = ? ORDER BY {$tabela1}.`id` DESC");
        $select->execute(array($id));

        $d['num'] = $select->rowCount();
        $d['dados'] = $select->fetchAll();

        return $d;
    }

    public static function SelectOne($tabela, $campo, $valor, $ordem = 'id DESC') {
        $select = self::getConn()->prepare("SELECT * FROM {$tabela} WHERE {$campo}=? ORDER BY {$ordem}");
        $select->execute(array($valor));

        $d['num'] = $select->rowCount();
        $d['dados'] = $select->fetchAll();

        return $d;
    }

    public static function SelectTwoMore($tabela, $campo, $ordem = 'id DESC') {
        $select = self::getConn()->prepare("SELECT * FROM {$tabela} WHERE {$campo} ORDER BY {$ordem}");
        $select->execute();

        $d['num'] = $select->rowCount();
        $d['dados'] = $select->fetchAll();

        return $d;
    }

    public static function Delete($tabela, $campo, $id = 0) {
        $delete = self::getConn()->prepare("DELETE FROM {$tabela} WHERE {$campo}=?");
        $delete->execute(array($id));

        return $delete;
    }

    public static function SelectJoin($tabela, $inner, $where, $ordem = 'id DESC') {
        $select = self::getConn()->prepare("SELECT * FROM {$tabela} {$inner} WHERE {$where} ORDER BY {$ordem}");
        $select->execute();

        $d['num'] = $select->rowCount();
        $d['dados'] = $select->fetchAll();

        return $d;
    }

    public static function SelectExtra($sql) {
        $select = self::getConn()->prepare($sql);
        $select->execute();

        $d['num'] = $select->rowCount();
        $d['dados'] = $select->fetchAll();

        return $d;
    }

    public static function SelectExtraLike($sql, $valor = '') {
        $select = self::getConn()->prepare($sql);
        if (empty($valor)) {
            $select->execute();
        } else {
            $select->execute(array("%{$valor}%"));
        }

        $d['num'] = $select->rowCount();
        $d['dados'] = $select->fetchAll();

        return $d;
    }

    public static function DeleteImagem($tabela, $pasta, $id) {
        $img = self::SelectOne($tabela, 'id', $id);
        // return $img['dados'][0]['imagem'];
        unlink($pasta . '/' . $img['dados'][0]['imagem']);
    }

    public static function SelectLimit($tabela, $limit = 12, $offset = 0, $ordem = 'id DESC') {
        $select = self::getConn()->prepare("SELECT * FROM {$tabela} ORDER BY {$ordem} LIMIT :lim OFFSET :off");
        $select->bindParam(':lim', $limit, PDO::PARAM_INT);
        $select->bindParam(':off', $offset, PDO::PARAM_INT);
        $select->execute();

        $d['num'] = $select->rowCount();
        $d['dados'] = $select->fetchAll();

        return $d;
    }

    public static function UpdateAjax($tabela, $dados) {
        $insert = self::getConn()->prepare("UPDATE {$tabela} SET {$dados}");
        $insert->execute();

        $lastId = self::getConn()->lastInsertId();
        return $lastId;
    }
    
    public static function GerarCodigoEmail($id) {
        $codigo = time();
        $codigo = base64_encode($codigo);
        $codigo = substr($codigo, 10, 4);
        $codigo = str_pad($codigo, 4, "x");
        $codigo .= base64_encode($id);
        $codigo = base64_encode($codigo);
        return $codigo;
    }

    public static function ReverterCodigoEmail($codigo) {
        $id = base64_decode($codigo);
        $id = substr($id, 4);
        $id = base64_decode($id);
        return $id;
    }

}
