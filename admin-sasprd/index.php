<?php
header("X-Robots-Tag: noindex", true);

ob_start();
session_start();
include("../classes/config.php");
include("../classes/DB.class.php");
include("../classes/Login.class.php");
if (isset($_SESSION[SESSION_ADMIN . '_uid'])) {
    header('Location: dashboard.php');
}
?>
<!DOCTYPE html>
<html lang="pt-br">


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.ico">
        <title>Curso de Avaliação da Aprendizagem</title>

        <!-- page css -->
        <link href="dist/css/pages/login-register-lock.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="dist/css/style.min.css" rel="stylesheet">
        <link href="dist/css/login.css?v=1.1" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <div class="loader">
                <div class="loader__figure"></div>
                <p class="loader__label">Curso de Avaliação da Aprendizagem</p>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <section id="wrapper" class="login-register login-sidebar" style="background-image:url(../images/banner.jpg);">
            <?php //error_reporting(E_ERROR | E_WARNING | E_PARSE); ?>
            <div class="banner text-center">
                
            </div>
            <div class="login-box card">            
                <div class="card-body">
                    <form class="form-horizontal form-material text-center" id="loginform" action="#" method="post">
                        <a href="javascript:void(0)" class="db"><img width="200px" src="assets/images/sas-logo.png" alt="Home"  style="padding-bottom: 5px;"/>
                        <div class="form-group m-t-40">
                            <div class="col-xs-12">
                                <input class="form-control" type="text" name="login" required="" placeholder="Login">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12 local-senha">
                                <input class="form-control" type="password" id="senha" name="senha" required="" placeholder="Senha">
                                <i class="fa fa-eye ver-senha texto-formulario"></i>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="d-flex no-block align-items-center">                                    
                                    <div class="">
                                        <a href="javascript:void(0)" id="to-recover" class="text-muted"><i class="fas fa-lock m-r-5"></i> Esqueceu sua senha?</a> 
                                    </div>
                                </div>   
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-success btn-lg btn-block text-uppercase btn-rounded" type="submit" name="entrar">Logar</button>
                            </div>
                        </div>
                        <!-- <div class="form-group m-b-0">
                            <div class="col-sm-12 text-center">
                                Don't have an account? <a href="pages-register2.html" class="text-primary m-l-5"><b>Sign Up</b></a>
                            </div>
                        </div> -->
                        <?php
                        if (isset($_POST['entrar'])) {
                            extract($_POST);
                            $logar = Login::logar($login, $senha);
                            if ($logar == true) {
                                header('Location: dashboard.php');
                            } else {
                                echo '<p style="color:red;"><strong>Usuário ou senha inválidos</strong></p>';
                            }
                        }
                        ?>
                    </form>
                    <form class="form-horizontal" id="recoverform" action="">
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <h3>Recuperar Senha</h3>
                                <p class="text-muted">Coloque o seu email para resetar a sua senha! </p>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" id="email-reset-senha" type="email" required="" placeholder="Email">
                            </div>
                            <div style="display: none;" class="form-group has-danger alerta-email-branco">     
                                <div class="form-control-feedback">O e-mail não pode ficar em branco!</div>
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="button" id="botao-reset-senha">Resetar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>

        <div id="modal_mensagem" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modal_mensagem_titulo"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <p id="modal_mensagem_texto"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Wrapper -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- All Jquery -->
        <!-- ============================================================== -->
        <script src="assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
        <!-- Bootstrap tether Core JavaScript -->
        <script src="assets/node_modules/popper/popper.min.js"></script>
        <script src="assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
        <!--Custom JavaScript -->
        <script type="text/javascript">
            $(function () {
                $(".preloader").fadeOut();
            });
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });
            // ============================================================== 
            // Login and Recover Password 
            // ============================================================== 
            $('#to-recover').on("click", function () {
                $("#loginform").slideUp();
                $("#recoverform").fadeIn();
            });

            $('#botao-reset-senha').on("click", function () {
                var email = $('#email-reset-senha').val();

                if (email == '') {
                    $(".alerta-email-branco").css("display", "block");
                } else {
                    $(".preloader").fadeIn();
                    $('#email-reset-senha').val('');
                    $.ajax({
                        type: "POST",
                        url: "js/ajax/resetar_senha.php",
                        data: {email: email},
                        success: function (response) {
                            $(".preloader").fadeOut();
                            if (response.resposta == 'ok') {
                                $('#modal_mensagem_titulo').text('Sucesso!');
                            } else {
                                $('#modal_mensagem_titulo').text('Falha!');
                            }
                            $('#modal_mensagem_texto').text(response.mensagem);
                            $('#modal_mensagem').modal();
                            $("#loginform").fadeIn();
                            $("#recoverform").slideUp();
                        },
                        error: function (response) {
                            $(".preloader").fadeOut();
                            $('#modal_mensagem_titulo').text('Falha!');
                            $('#modal_mensagem_texto').text('Não foi possível resetar a senha.');
                            $('#modal_mensagem').modal();
                        }
                    });
                }

                $('#email-reset-senha').keyup(function (event) {
                    if ($(this).val().length > 0) {
                        $(".alerta-email-branco").css("display", "none");
                    }
                });
            });
        </script>
        
        <style>
            form .local-senha {
                position: relative !important;
            }
            form .ver-senha {
                /*opacity: 0.4;*/
                color: #6c757d!important;
                font-size: 18px !important;
                cursor: pointer !important;
                position: absolute !important;
                top: 10px !important;
                right: 15px !important;
                z-index: 99 !important;
            }
        </style>
        <script type="text/javascript">
            $(document).on('click','.ver-senha', function (e) {
                if ($(this).hasClass('fa-eye')) {
                    $(this).removeClass('fa-eye')
                    $(this).addClass('fa-eye-slash')
                    $(this).siblings('#senha').attr('type', 'text');
                    $(this).siblings('#senha').focus();
                } else {
                    $(this).removeClass('fa-eye-slash')
                    $(this).addClass('fa-eye')
                    $(this).siblings('#senha').attr('type', 'password');
                    $(this).siblings('#senha').focus();
                }     
            });  
        </script>

    </body>

</html>