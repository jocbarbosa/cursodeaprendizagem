<?php 
include_once ('./include.php');

$registros_dia = 100;

extract($_POST);
 
if (!isset($data)) {
    $data = '';
}
if (!isset($escola)) {
    $escola = '';
}
if (!isset($perfil) || empty($perfil)) {
    $perfil = 'usuarios';
}
if (!isset($acao) || empty($acao)) {
    $acao = 'incritos';
}

$dados = Dados::incritos($data, $escola, '', 'total');

$total = ceil($dados['dados'][0]['total'] / $registros_dia);  
?>

<div class="m-t-20 local-dados">
    <div class="dia-dados">
        <div class="progress m-b-20" style="height: 50px; opacity: 0.3;">
            <div class="progress-bar progress-bar-striped bg-info progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%; height: "></div>
        </div>
    </div>    
    <?php if ($total > 1) { ?>
        <nav class="paginacao-longa" aria-label="...">
            <ul class="pagination">
                <?php for ($i = 1; $i <= $total; $i++) { ?>
                    <li class="page-item <?php $i == 1 ? 'active' : '' ?> dias" data-pagina="<?php echo $i ?>">
                        <a class="page-link" href="#"><strong><?php echo $i ?></strong></a>
                    </li>
                <?php } ?>                                       
            </ul>
        </nav>
    <?php } ?>
</div>

<style></style>

<?php //echo '<pre>'; print_r($_POST); echo '</pre>'; */ ?>

<style></style>

<script type="text/javascript">
    $(document).ready(function () {
        var dados = <?php echo json_encode($_POST) ?>;
        dados['pagina'] = 1;
        $.ajax({
            type: "POST",
            url: "js/ajax/dia_dados.php",
            data: dados,
            success: function (resposta) {
                $('.dia-dados').html(resposta);
            }
        });
    });
    
    $('.page-item.dias').on('click', function (event) {
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
        event.preventDefault();
        var dados = <?php echo json_encode($_POST) ?>;
        var pagina = $(this).data('pagina');        
        dados['pagina'] = pagina;
        $.ajax({
            type: "POST",
            url: "js/ajax/dia_dados.php",
            data: dados,
            success: function (resposta) {
                $('.dia-dados').html(resposta);
            }
        }); 
    }); 
</script>