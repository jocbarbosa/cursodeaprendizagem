<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">                
                <?php if (isset($_SESSION[SESSION_ADMIN . '_uid'])) { ?>
                    <?php if (empty($minha_bandeira)) { ?>
                    <li> <a class="waves-effect waves-dark" href="dashboard.php" aria-expanded="false"><i class="ti-stats-up"></i><span class="hide-menu">Início</span></a> </li>
                    <li> 
                        <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="ti-unlock"></i><span class="hide-menu">Administradores</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="lista_usuarios.php">Todos</a></li>
                            <li><a href="usuario.php">Novo</a></li>
                        </ul>
                    </li>
                    <li> 
                        <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="ti-bookmark-alt"></i><span class="hide-menu">Escolas</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="lista_escolas.php">Todas</a></li>
                            <li><a href="escola.php">Nova</a></li>
                        </ul>
                    </li>
                    <li> <a class="waves-effect waves-dark" href="lista_cursistas.php" aria-expanded="false"><i class="icon-people"></i><span class="hide-menu">Inscritos</span></a> </li>
                    <?php } ?>
                    <li> <a class="waves-effect waves-dark" href="sair.php" aria-expanded="false"><i class="icon-action-undo"></i><span class="hide-menu">Sair</span></a> </li>
                <?php } ?>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->