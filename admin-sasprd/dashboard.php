<?php
    include('./templates/header.php');
    ?>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <?php include('templates/topo.php'); ?>
        <?php include('templates/menu.php'); ?>

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Informações</h4>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <?php     
                                    $incricoes = Dados::dias(timesdata($data_inicio));  
                                    
                                    $total_incricoes = 0;
                                    $media_incricoes = 0;
                                    $hoje_incricoes = 0;
                                    
                                    $total_dias = count($incricoes);
                                    if ($total_dias > 0) {
                                        foreach ($incricoes as $incricao) {
                                            $total_incricoes += $incricao['total'];
                                        }
                                        $media_incricoes = ceil($total_incricoes / $total_dias);
                                        $hoje_incricoes = $incricoes[($total_dias -1)]['total'];
                                    }
                                ?>
                                <h4 class="card-title">Acesso e Inscrições</h4>
                                <div id="grafico-dias" class="grafico-dias"></div>
                            </div>
                        </div>
                    </div>
                </div>   
                
                <div class="row">
                    <div class="col-md-4" data-campo="cadastro" data-termo="todos">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="round align-self-center round-success dados-acessos" style="background-color: #d2a200;"><i class="icon-people"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0"><?php echo $total_incricoes ?></h3>
                                        <h5 class="text-muted m-b-0">Incrições</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="col-md-4" data-campo="cadastro" data-termo="todos">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="round align-self-center round-success dados-inscricoes" style="background-color: #698C0A;"><i class="ti-bar-chart"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0"><?php echo $media_incricoes ?></h3>
                                        <h5 class="text-muted m-b-0">Média diária</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4" data-campo="cadastro" data-termo="todos">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="round align-self-center round-success dados-iniciaram" style="background-color: #113F8C;"><i class="ti-calendar"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0"><?php echo $hoje_incricoes ?></h3>
                                        <h5 class="text-muted m-b-0">Inscritos Hoje</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>

                <div class="row">
                    <div class="col-lg-12">                        
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <h4 class="card-title m-b-0 m-t-1">Distribuição por Escola</h4><br>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected m-b-10">
                                            <input id="pesquisar-escola" type="text" data-bts-button-up-class="btn btn-secondary btn-outline" class="form-control" placeholder="Procurar escola">
                                            <span class="input-group-btn input-group-append">
                                                <button class="btn btn-secondary btn-outline bootstrap-touchspin-up pesquisar-escola" type="button">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>    
                                <div class="table-responsive">
                                    <div class="local-escolas"></div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <?php include('templates/sidebar.php'); ?>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    <form id="pesquisa_info" method="post" action="procurar_aluno.php">
        <input type="hidden" id="campo_info" name="campo">
        <input type="hidden" id="termo_info" name="termo">
    </form>  
        
    <div id="modal_detalhes" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="round align-self-center round-success dados-acessos"><i class="icon-people"></i></div>
                    <h4 class="modal-title">Detalhes das inscrições do dia <strong></strong></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form id="form-dia" method="post" action="" class="p-t-10">
                        <input type="hidden" id="data" name="data">                   
                        <div class="row">                                                
                            <div class="col-md-12">
                                <select class="form-control" id="perfil" name="perfil">
                                    <option value="usuarios" class="inicial">Usuarios</option>
                                    <option value="escolas">Escolas</option>
                                </select>
                            </div>                                          
                        </div>
                    </form>                    
                    <div class="row">
                        <div class="col-md-12 dados">xxxxxxxxxxxxxxxxxxx</div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn waves-effect" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
        
    <div id="modal_detalhes_geral" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="round align-self-center round-success acessos"><i class="icon-people"></i></div>
                    <h4 class="modal-title"><strong></strong> - detalhes</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form id="form-geral" method="post" action="" class="p-t-10">
                        <input type="hidden" id="acao" name="acao">                         
                        <div class="row">                                                
                            <div class="col-md-12">
                                <select class="form-control" id="perfil" name="perfil">
                                    <option value="usuarios" class="inicial">Usuarios</option>
                                    <option value="escolas">Escolas</option>
                                </select>
                            </div>                                          
                        </div>
                    </form>                    
                    <div class="row">
                        <div class="col-md-12 dados"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn waves-effect" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

<?php include('templates/footer.php'); ?>  
<!-- agora <?php echo time() ?> -->
<script type="text/javascript">
    $(document).ready(function () {
        setTimeout(function(){ 
            $('.sidebartoggler').click(); 
            $('.detalhes').click();
            setTimeout(function(){ 
                $('.detalhes').click();
            }, 500); 
        }, 100);   
    });  
    /*
    $('form#form-dia select#acao').on('change', function (e) {                
        var acao = $('form#form-dia select#acao option:selected').val();
        $('#modal_detalhes .modal-header .round').removeClass('incritos');
        $('#modal_detalhes .modal-header .round i').removeClass('icon-note');                
        $('#modal_detalhes .modal-header .round').removeClass('iniciaram');
        $('#modal_detalhes .modal-header .round i').removeClass('icon-screen-desktop');                
        $('#modal_detalhes .modal-header .round').removeClass('concluiram');
        $('#modal_detalhes .modal-header .round i').removeClass('icon-graduation');                
        $('#modal_detalhes .modal-header .round').addClass(acao);               
        if (acao == 'incritos') {
            $('#modal_detalhes .modal-header .round i').addClass('icon-note');   
        }
        if (acao == 'iniciaram') {
            $('#modal_detalhes .modal-header .round i').addClass('icon-screen-desktop');
        }
        if (acao == 'concluiram') {
            $('#modal_detalhes .modal-header .round i').addClass('icon-graduation');
        }
    });            
    */
    $('form#form-dia select').on('change', function (e) {
        $.ajax({
            type: "POST",
            url: "js/ajax/dia.php",
            data: $('form#form-dia').serialize(),
            success: function (resposta) {
                $('#modal_detalhes .dados').html(resposta);
            }
        }); 
    });
    
    $('.dados-acessos').on('click', function (event) {
        
        $('#form-geral select option').removeAttr('selected');
        $('#form-geral select option.inicial').attr('selected', 'selected');
        $('#modal_detalhes_geral .modal-header .modal-title strong').text('Se Inscreveram');
        
        $.ajax({
            type: "POST",
            url: "js/ajax/dia.php",
            data: {perfil: 'usuarios', acao: 'incritos'},
            success: function (resposta) {
                $('#modal_detalhes_geral .dados').html(resposta);
                $('#modal_detalhes_geral').modal();
            }
        }); 
    });  
    
    $('form#form-geral select').on('change', function (e) {
        $.ajax({
            type: "POST",
            url: "js/ajax/dia.php",
            data: $('form#form-geral').serialize(),
            success: function (resposta) {
                $('#modal_detalhes_geral .dados').html(resposta);
            }
        }); 
    });
    
    $('button.pesquisar-escola').on('click', function (e) {
        var unidade = '<?php echo isset($_POST['unidade']) ? $_POST['unidade'] : '' ?>';
        var escola = $('#pesquisar-escola').val();   
        $.ajax({
            type: "POST",
            url: "js/ajax/home_escolas.php",
            data: {unidade: unidade, escola: escola, pagina: 1},
            success: function (resposta) {
                $('.local-escolas').html(resposta);
            }
        });
    });
    
    $('input#pesquisar-escola').on('keydown', function (e) {
        if (e.keyCode === 13) {
            var unidade = '<?php echo isset($_POST['unidade']) ? $_POST['unidade'] : '' ?>';
            var escola = $('#pesquisar-escola').val();   
            $.ajax({
                type: "POST",
                url: "js/ajax/home_escolas.php",
                data: {unidade: unidade, escola: escola, pagina: 1},
                success: function (resposta) {
                    $('.local-escolas').html(resposta);
                }
            });
        }
    });
    
    var unidade = '<?php echo isset($_POST['unidade']) ? $_POST['unidade'] : '' ?>';
    $.ajax({
        type: "POST",
        url: "js/ajax/home_escolas.php",
        data: {unidade: unidade, pagina: 1},
        success: function (resposta) {
            $('.local-escolas').html(resposta);
        }
    });
    
    
    
    $(document).ready(function() {

        <?php if (isset($incricoes) && ! empty($incricoes)) {  ?>
            Morris.Line({
                element: 'grafico-dias',
                data: <?php echo json_encode($incricoes) ?>,
                xkey: 'dia',
                ykeys: ['total'],
                labels: ['Incrições'],
                pointSize: 5,
                fillOpacity: 0.4,
                behaveLikeLine: true,
                gridLineColor: '#e0e0e0',
                lineWidth: 3,
                smooth: false,
                hideHover: 'auto',
                lineColors: ['#d2a200','#698C0A','#113F8C', '#DB1F01'],
                resize: true,
                parseTime: false,       
                xLabelMargin:  10,         
            }).on('click', function (i, dados) {                 
                var data = dados.dia;                           
                $('#form-dia select option').removeAttr('selected');
                $('#form-dia select option.inicial').attr('selected', 'selected');			
                $('#modal_detalhes .modal-title strong').html(data);
                $('form#form-dia #data').val(data);               
                $.ajax({
                    type: "POST",
                    url: "js/ajax/dia.php",
                    data: {data: data, perfil: 'usuarios'},
                    success: function (resposta) {
                        $('#modal_detalhes .dados').html(resposta);
                        $('#modal_detalhes').modal();
                    }
                });                     
            }); 
        <?php } ?>  
    }); 
    
</script>