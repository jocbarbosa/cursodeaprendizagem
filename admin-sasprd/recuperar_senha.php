<?php
include('../classes/config.php');
include('../classes/DB.class.php');
include('../classes/CRUD.class.php');
include('../classes/class.upload.php');
include('../classes/PHPMailerAutoload.php');
extract($_GET);

if (isset($codigo)) {
    $id = CRUD::ReverterCodigoEmail($codigo);
    $cadastro = CRUD::SelectOne('administradores','id',$id);    
} else {
    header('Location: '.$url_site.'index.php');
}

echo "<pre>"; print_r($_POST); echo "</pre>";
?>
<!DOCTYPE html>
<html lang="pt-br">
    <!-- Mirrored from eliteadmin.themedesigner.in/demos/bt4/crm/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 19 Jul 2018 17:39:01 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
        <title>Recuperar Senha - Congresso Cordimariano</title>    
        <!-- page css -->
        <link href="dist/css/pages/login-register-lock.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="dist/css/style.min.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="skin-default card-no-border">
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <section id="wrapper">
            <div class="login-register">
                <div class="login-box card">
                    <div class="card-body">                    
                        <form class="form-horizontal text-center" id="recuperarsenha" method="post" action="">
                            <img src="images/logo_03.png" alt="Home" style="width:300px; margin-top: 30px"/></a>
                            <br/><br/>
                            <div class="form-group ">
                                <div class="col-xs-12">
                                    <h3>Nova Senha</h3>
                                    <p class="text-muted">Cadastre uma nova senha para seu usuário </p>
                                </div>
                            </div>
                            <div class="input-group">
                                <input type="password" id="novasenha" name="novasenha" class="form-control" minlength=3 maxlength=20>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i style="cursor: pointer;" class="fa fa-eye versenha"></i>
                                        <i style="display: none; cursor: pointer;" class="fa fa-eye-slash escondersenha"></i>
                                    </span>
                                </div>
                            </div>
                            <div style="display: none;" class="form-group has-danger alerta-senha-branco">     
                                <div class="form-control-feedback">A senha não pode ficar em branco!</div>
                            </div>
                            <div style="display: none;" class="form-group has-danger alerta-senha-minimo">     
                                <div class="form-control-feedback">A senha deve ter no mínimo 3 caracteres!</div>
                            </div>
                            <div style="display: none;" class="form-group has-danger alerta-senha-maximo">     
                                <div class="form-control-feedback">A senha deve ter no máximo 20 caracteres!</div>
                            </div>
                            <div style="display: none;" class="form-group has-danger alerta-usuario">     
                                <div class="form-control-feedback">Aluno não encontrado!</div>
                            </div>
                            <div class="form-group text-center m-t-20">
                                <div class="col-xs-12">
                                    <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light confirmaalterasenha" type="button">Alterar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>       
        <div id="modal_mensagem" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modal_mensagem_titulo"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <p id="modal_mensagem_texto"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger waves-effect fechar" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div> 
        <!-- ============================================================== -->
        <!-- End Wrapper -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- All Jquery -->
        <!-- ============================================================== -->
        <script src="assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
        <!-- Bootstrap tether Core JavaScript -->
        <script src="assets/node_modules/popper/popper.min.js"></script>
        <script src="assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
        <!--Custom JavaScript -->
        <script src="js/senha.js"></script>   
        <script type="text/javascript">
            <?php 
            if ($cadastro['num'] == 0) { 
                ?>
                $('#modal_mensagem_titulo').text('Falha!');
                $('#modal_mensagem_texto').text('Usuário não localizado.');
                $('#modal_mensagem').modal();
                $(document).on('click', '.fechar', function (event) {
                    window.location.href = "index.php"; 
                });
                <?php 
            } else {
                if (isset($_POST['novasenha']) && !empty($_POST['novasenha'])) {
                    extract($_POST);
                    $id = $cadastro['dados'][0]['id'];
                    $_POST = array('senha' => $novasenha);
                    $alterar = CRUD::Insert('administradores', $id);
                    ?>
                    $('#modal_mensagem_titulo').text('Sucesso!');
                    $('#modal_mensagem_texto').text('Senha alterada com sucesso.');
                    $('#modal_mensagem').modal();
                    $(document).on('click', '.fechar', function (event) {
                        window.location.href = "index.php"; 
                    });
                    <?php 
                }
            } 
            ?>
        </script>         
    </body>
</html>