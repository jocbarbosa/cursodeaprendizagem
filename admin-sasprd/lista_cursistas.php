<?php
include('templates/header.php');
if (isset($minha_bandeira) && !empty($minha_bandeira)) {
    die();
}
$perfis = array(
    'Educador' => 'Educador',
    'Pedagogica' => 'Gestão Pedagógica',
    'Administrativo' => 'Administrativo',
);
?>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div id="main-wrapper">
    <?php include('templates/topo.php'); ?>
    <?php include('templates/menu.php'); ?>
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Cursistas</h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Cursistas</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Inscritos</h4>                            
                            <div class="m-t-20">
                                <?php
                                extract($_GET);
                                if (isset($id)) {
                                    $id = (int) $id;
                                    $escola = CRUD::SelectExtra("SELECT e.id, c.estado, c.cidade, escola, endereco, cep	 
                                                                 FROM escolas AS e LEFT JOIN cidades AS c ON (c.id = e.id_cidade)
                                                                 WHERE e.id = {$id}");
                                    if ($escola['num'] > 0) {
                                        ?><h4 class="p-b-30"><?php echo $escola['dados'][0]['escola'] ?> (<?php echo $escola['dados'][0]['cidade'] ?> / <?php echo $escola['dados'][0]['estado'] ?>)</h4><?php                                        
                                    }
                                } else {
                                    $id = '';
                                }
                                $dados = Dados::incritos('', $id, '', $perfil = 'usuarios');                                
                                if ($dados['num'] > 0) {
                                    ?>
                                    <table id="lista_escolas" class="display table table-striped table-hover" cellspacing="0" width="100%" data-table="escolas">
                                        <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th>Perfil</th>
                                                <th>E-mail</th>
                                                <th>Escola</th>
                                                <th>Cidade</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($dados['dados'] as $lista) {
                                                ?>
                                                <tr class="item<?php echo $lista['id'] ?>">
                                                    <td><?php echo $lista['nome'] ?></td>
                                                    <td><?php echo $perfis[$lista['perfil']] ?></td>
                                                    <td>
                                                        <?php $partes = explode('@', $lista['email']);?>
                                                        <?php echo $partes[0] ?><wbr>@<?php echo $partes[1] ?>
                                                    </td>
                                                    <td><?php echo $lista['escola'] ?><?php echo !empty($lista['outra_escola']) ? "Outras - ".$lista['outra_escola'] : '' ?></td>
                                                    <td><?php echo $lista['cidade'] ?> / <?php echo $lista['estado'] ?></td>
                                                </tr>
                                                <?php                                                
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="modal_mensagem" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel"></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <p></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <?php include('templates/sidebar.php'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
    <?php include('templates/footer.php'); ?>
    <script>
        
        $('#lista_escolas').DataTable({ 
            language: traducao, 
            searching: true,
            pageLength: 100,
            dom: 'Bfrtip',
            buttons: ['copy', { extend: 'csv', charset: 'UTF-8', fieldSeparator: ';', bom: true }, 'excel', 'pdf', 'print']
        });
        $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
    </script>

