<?php
session_start();
include('../../classes/config.php');
include('../../classes/db/DB.class.php');
include('../../classes/CRUD.class.php');
include('../../classes/class.upload.php');
include('../../classes/PHPMailerAutoload.php');
include('../../classes/Geral.class.php');
extract($_POST);

$configuracoes = CRUD::Configuracoes();

if ($senha == '654321') {
    $senha = "(SELECT valor FROM configuracoes WHERE configuracao = 'senha_padrao')";
} else {
    $senha = mb_convert_encoding($senha,'ASCII');
    $senha = hash('sha256', $senha, true);
    $senha = utf8_encode($senha);
    $senha = mb_convert_encoding($senha,'ASCII');
    $senha = "'".$senha."'";
}

try {
    CRUD::UpdateAjax('Alunos', "Senha = {$senha} WHERE id = '{$usuario}'");
} catch (Exception $e) {
    header("HTTP/1.0 400 Bad Request"); 
    echo 'Exceção capturada: ',  $e->getMessage(), "\n";
}


?>