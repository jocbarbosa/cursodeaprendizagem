# Curso de Avaliação de Aprendizagem - SAS

## Dependências
    PHP 7.4
    Mysql

## Instalação

    1 - Importar o arquivo .sql

    2 - Todas as alterações que precisam ser feitas, estão no arquivo classes/config.php: 
            Para configurar o banco de dados, alterar os dados:
                DB_HOST
                DB_NAME
                DB_USER
                DB_PASS
            Para configurar urls de redirecionamento internas, alterar:
                seu.dominio.com.br => Alterar para endereço onde site esta publicado. Ex: focos.saseducacao.com.br/cursodeaprendizagem
            Para configurar o envio de e-mail após a inscrição, alterar os dados de SMTP:
                MAIL_HOST
                MAIL_USER
                MAIL_PASS
                MAIL_PORT
                MAIL_SECURE => TLS or SSL
                MAIL_SEND => email remetente