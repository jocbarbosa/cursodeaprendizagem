<?php setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese'); ?>
<?php date_default_timezone_set('America/Sao_Paulo'); ?>
<!DOCTYPE html>
<?php include("./classes/config.php"); ?>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="robots" content="noindex">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Curso de Avaliação de Aprendizagem Escolar</title>
        <link rel="shortcut icon" href="images/favicon.ico"/>
        <meta name="description" content="Curso Avaliação da aprendizagem" />
        <meta name="robots" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
        <meta property="og:locale" content="pt_BR" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Curso Avaliação da aprendizagem" />
        <meta property="og:description" content="Curso Avaliação da aprendizagem" />
        <meta property="og:site_name" content="Curso Avaliação da aprendizagem" />
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
        <link rel="stylesheet" href="./assets/slick/slick.css">
        <link rel="stylesheet" href="./assets/slick/slick-theme.css">        
        <link rel="stylesheet" href="css/carrossel.css">        
        <link rel="stylesheet" href="css/animate.css">         
        <link rel="stylesheet" href="css/select2.min.css">         
        <link rel="stylesheet" href="https://use.typekit.net/oyf2kru.css">   
        <link rel="stylesheet" href="css/style.css?v=1.6">
        <link rel="stylesheet" href="css/cores.css?v=1.5">
        <link rel="stylesheet" href="css/responsivo.css?v=1.5">
    </head>

    <div class="loading ativo">
        <div class="animated unload base fundo-loading" data-animation="fadeOut" data-delay="10"></div>
        <img src="images/sas.svg" id="tempo" style="width: 100px;">
    </div>

    <body>   
        <header class="borda-secundario main-header">
            <div class="container">
                <div class="row">
                    <div class="offset-lg-8 col-lg-3 text-center buttons">
                        <h6 class="inscrever fundo-primario texto-destaque">Inscreva-se</h6>     
                        <h6 style="background-color: blue !important; color: #FFF !important;" class="cadastrado fundo-destaque">
                            <a style="background-color: blue !important; color: #FFF !important;" target="_blank" href="https://focos.saseducacao.com.br" class="texto-primario">Sou cadastrado!</a>
                        </h6>
                    </div>
                </div>
                <div class="row local-titulo">
                    
                </div>   
                
            </div>
        </header>
        
        <section id="parceiros">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h5 class="texto-secundario">Curso disponível de <strong class="texto-primario"><?php echo $data_inicio ?> a <?php echo $data_termino ?></strong></h5>
                        <h4 class="texto-primario"><strong class="texto-secundario">Inscrições:</strong> a partir de <?php echo $data_incricao ?> a <?php echo $data_termino_inscricao ?> (ou até encerrarmos as vagas)</h4>
                    </div>
                </div>
            </div>
        </section>

        <section id="apresentacao">
            <div class="container">
                <div class="row" id="avaliar-para-que">
                    <!-- <div class="offset-lg-12 col-lg-12">
                       
                    </div>-->
                </div>
                <div class="row mt-4 mb-5">
                    <div class="offset-lg-2 col-lg-8">
                        <p class="texto-cinza">
                            Durante o curso você será levado a refletir sobre o que a Avaliação representa no seu contexto e na realidade dos seus alunos. Desse modo irá compreender de onde está partindo e onde quer chegar para que suas avaliações não sejam apenas notas.
                        </p>
                        <p class="texto-cinza">
                            Ao final do curso, você estará ainda mais preparado para responder às perguntas:
                            Avaliar para quê? Como avaliar? O que fazer com os resultados?

                            Acreditamos que este curso será uma importante ferramenta de transformação do seu trabalho em sala de aula. Portanto, dedique-se!
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section id="numeros" class="fundo-primario">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h4 class="fundo-secundario texto-branco conteudo-label">CONTEÚDO PROGRAMÁTICO</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="offset-lg-1 col-lg-12 local-texto">
                        <p class="texto-branco">
                            <strong>Módulo 1 : </strong> Introdução: Avaliar para que?
                        </p>
                    </div>
                </div>
                <div class="row">
                  
                    <div class="offset-lg-1 col-lg-12 local-texto">
                        <p class="texto-branco">
                            <strong>Módulo 2 : </strong> Como Avaliar? Avaliação e seus contextos
                        </p>
                    </div>
                </div>
                <div class="row">
                   
                    <div class="offset-lg-1 col-lg-12 local-texto">
                        <p class="texto-branco">
                        <strong>Módulo 3 : </strong> Avaliei, e agora? O que fazer com os resultados
                        </p>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="offset-lg-1 col-lg-12 local-texto">
                        <p class="texto-branco">
                        <strong>Módulo 4 : </strong> Mergulhando fundo: Avaliação nos segmentos da Educação Básica
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section id="sobre">
            <div class="container">
                <div class="row" id="sobre-image">
                    <div class="col-lg-12 text-center">
                        
                    </div>
                </div>
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <p class="texto-cinza">
                            O curso Avaliação da Aprendizagem Escolar foi elaborado pelos melhores especialistas do SAS, pensando em ajudar você nos desafios que permeiam essa temática no processo de ensino e aprendizagem.
                        </p>
                        <p class="texto-cinza">
                            Ao longo de 4 módulos, você terá acesso a um conteúdo com a excelência SAS em diferentes formatos, como: videoaulas objetivas e dinâmicas na estrutura de bate-papo, eBook, infográfico, podcast e atividades avaliativas.
                            Potencializar a sua prática em sala de aula: essa é a nossa missão!
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section id="preparese">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div>
                            <h2 class="text-center">
                                <span class="texto-secundario preparese">Não Perca tempo</span>
                                <span class="texto-primario">
                                    <span class="faca-sua-inscricao">faça sua inscrição</span>
                                </span>
                            </h2>
                        </div>                        
                    </div>                    
                </div>
            </div>
        </section>

        <section id="formulario">
            <div class="container">
                <div class="row">
                    <div class="offset-lg-2 col-lg-8">
                    <form method="post" action="cadastrar.php" id="inscricao" class="needs-validation" autocomplete="off">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h3 class="texto-primario">Inscreva-se e transforme o processo de avaliação da sua escola!</h3>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="nome" class="texto-formulario">Nome Completo</label>
                                        <input type="text" class="form-control borda-formulario" id="nome" name="nome" required>
                                    </div>
                                </div> 
                            </div>
                            <div>
                                <div class="col-lg-12">
                                    <small style="text-align: justify"><strong>Digite seu email com atenção, pois ele também será sua senha e nosso canal de comunicação para que você possa receber informações sobre o curso.</strong></small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="email" class="texto-formulario">E-mail</label>
                                        <input type="email" class="form-control borda-formulario" id="email" name="email" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="email2" class="texto-formulario">E-mail</label>
                                        <input onchange="checkEmailConfirmation()" type="email" class="form-control borda-formulario" id="email2" name="email2" required>
                                    </div>
                                    <small id="mensagem-email-diferente" style="display: none; color: #ba2032">Os e-mails devem ser iguais.</small>
                                </div>
                                <div class="col-lg-6" style="display:none">
                                    <div class="form-group">
                                        <label for="senha" class="texto-formulario">Senha <small class="texto-formulario">(mínimo de  6 caracteres)</small></label>
                                        <div class="local-senha">
                                            <input type="password" class="form-control borda-formulario" id="senha" name="senha" value="jd8732hlsd17jk" required>
                                            <i class="fa fa-eye ver-senha texto-formulario"></i>
                                        </div>                                                                            
                                    </div>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="telefone" class="texto-formulario">Telefone</label>
                                        <input type="tel" class="form-control borda-formulario" id="telefone" name="telefone" required>
                                    </div>
                                </div> 
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="perfil" class="texto-formulario">Perfil</label>
                                        <select class="form-control borda-formulario" id="perfil" name="perfil" required>
                                            <option value=""></option>
                                            <option value="Educador">Educador</option>
                                            <option value="Pedagogica">Gestão Pedagógica</option>
                                            <option value="Administrativo">Administrativo</option>
                                        </select>
                                    </div>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="estado" class="texto-formulario">Estado</label>
                                        <select class="form-control borda-formulario" id="estado" name="estado" required>
                                            <option value=""></option>
                                            <?php foreach ($estadosBrasileiros as $sigla => $estado) { ?>
                                                <option value="<?php echo $sigla ?>"><?php echo $estado ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div> 
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="cidade" class="texto-formulario">Cidade</label>
                                        <select class="form-control borda-formulario" id="cidade" name="cidade" required></select>
                                    </div>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="escola" class="texto-formulario">Escola</label>
                                        <select class="form-control borda-formulario" id="escola" name="escola" required></select>
                                    </div>
                                </div> 
                            </div>
                            <div class="row" id="local_outra_escola">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="outra_escola" class="texto-formulario">Nome da Escola</label>
                                        <input type="text" class="form-control borda-formulario" id="outra_escola" name="outra_escola">
                                    </div>
                                </div> 
                            </div>                            
                            <div class="row">
                                <div class="col-lg-6 text-center">
                                    <div class="form-check mt-2">                                        
                                        <label class="form-check-label texto-cinza" for="concordo">
                                            <input type="checkbox" class="form-check-input" id="concordo" required>
                                            Aceito os <a data-toggle="modal" href="#modal-dados">termos de uso</a>
                                        </label>
                                    </div>
                                </div> 
                                <div class="col-lg-6 text-center">
                                    <div class="form-check mt-2">                                        
                                        <label class="form-check-label texto-cinza" for="uso">
                                            <input type="checkbox" class="form-check-input" id="uso" required>
                                            Aceito o <a data-toggle="modal" href="#modal-dados">uso de dados</a>
                                        </label>
                                    </div>
                                </div> 
                            </div>                            
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <button type="submit" class="btn btn-primary fundo-secundario borda-secundario">Enviar formulário de inscrição</button>
                                </div> 
                            </div>
                        </form>
                    </div>
                </div>                
            </div>
        </section>  
        
        <div id="modal-resposta" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">                
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span> Fechar
                        </button>
                    </div>
                    <div class="modal-body">
                        <img src="images/focos-white.svg" width="497px" class="img-fluid">
                        <div class="mensagem"></div>  
                    </div>
                    <img src="images/numeros-base.svg">
                </div>
            </div>
        </div>
        
        <div id="modal-termos" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span> Fechar
                        </button>
                    </div>
                    <div class="modal-body">
                        <?php echo file_get_contents("./js/ajax/termos.html"); ?>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="modal-dados" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span> Fechar
                        </button>
                    </div>
                    <div class="modal-body">
                        <?php echo file_get_contents("./js/ajax/dados.html"); ?>
                    </div>
                </div>
            </div>
        </div>

        <footer>
            <div class="fundo-primario">
                <div class="container">
                    <div class="row">
                        <div class="offset-lg-2 col-lg-8">
                            <table width="100%" class="logos logo-branco">
                                <tr>
                                    <td class="sas">
                                        <svg id="logo-sas" data-name="logo-sas" xmlns="http://www.w3.org/2000/svg" width="153" height="84" viewBox="0 0 153 84" fill="none">
                                            <path d="M0 73.1843H3.45064C5.46894 73.1843 6.77106 74.3501 6.77106 76.0987C6.77106 78.0417 5.20851 79.0779 3.25532 79.0779H1.56255V81.8628H0V73.1843ZM3.32043 77.7179C4.49234 77.7179 5.20851 77.0702 5.20851 76.1635C5.20851 75.1273 4.49234 74.6092 3.32043 74.6092H1.56255V77.7826H3.32043V77.7179Z" fill="white"></path><path d="M8.33362 72.7959H9.83107V81.9277H8.33362V72.7959Z" fill="white"></path><path d="M15.886 81.9276V81.0857C15.4302 81.6038 14.714 82.0571 13.7374 82.0571C12.5004 82.0571 11.3936 81.3447 11.3936 80.0494C11.3936 78.5598 12.5655 77.9122 14.063 77.9122C14.8443 77.9122 15.3651 78.0417 15.886 78.1713V78.0417C15.886 77.135 15.3 76.6169 14.2583 76.6169C13.5421 76.6169 12.9562 76.8112 12.3702 77.0055L11.9796 75.775C12.6957 75.4511 13.4119 75.2568 14.5187 75.2568C16.4719 75.2568 17.4485 76.2931 17.4485 78.0417V81.9276H15.886ZM15.9511 79.1427C15.5604 79.0132 15.0396 78.8837 14.4536 78.8837C13.477 78.8837 12.9562 79.2723 12.9562 79.9199C12.9562 80.5676 13.5421 80.8914 14.2583 80.8914C15.2349 80.8914 16.0162 80.3085 16.0162 79.5313V79.1427H15.9511Z" fill="white"></path><path d="M19.4668 80.1143V76.617H18.6204V75.3217H19.4668V73.5083H20.9643V75.3217H22.7872V76.617H21.0294V79.8553C21.0294 80.4381 21.3549 80.6972 21.8757 80.6972C22.2013 80.6972 22.5268 80.6324 22.7872 80.5029V81.7334C22.3966 81.9277 22.006 82.0573 21.42 82.0573C20.2481 82.0573 19.4668 81.5391 19.4668 80.1143Z" fill="white"></path><path d="M28.3864 81.9276V81.0857C27.9306 81.6038 27.2145 82.0571 26.2379 82.0571C25.0009 82.0571 23.894 81.3447 23.894 80.0494C23.894 78.5598 25.066 77.9122 26.5634 77.9122C27.3447 77.9122 27.8655 78.0417 28.3864 78.1713V78.0417C28.3864 77.135 27.8004 76.6169 26.7587 76.6169C26.0426 76.6169 25.4566 76.8112 24.8706 77.0055L24.48 75.775C25.1962 75.4511 25.9123 75.2568 27.0191 75.2568C28.9723 75.2568 29.9489 76.2931 29.9489 78.0417V81.9276H28.3864ZM28.3864 79.1427C27.9957 79.0132 27.4749 78.8837 26.8889 78.8837C25.9123 78.8837 25.3915 79.2723 25.3915 79.9199C25.3915 80.5676 25.9774 80.8914 26.6936 80.8914C27.6702 80.8914 28.4515 80.3085 28.4515 79.5313V79.1427H28.3864Z" fill="white"></path><path d="M32.0324 76.6168H31.186V75.3215H32.0324V74.8682C32.0324 74.1558 32.2277 73.5729 32.5532 73.2491C32.8787 72.9252 33.3996 72.731 34.0507 72.731C34.5715 72.731 34.9621 72.7957 35.2877 72.9252V74.2205C34.9621 74.091 34.7017 74.0263 34.3762 74.0263C33.7902 74.0263 33.4647 74.3501 33.4647 74.9977V75.3863H35.2226V76.6168H33.4647V81.9276H31.9672V76.6168H32.0324Z" fill="white"></path><path d="M35.9387 78.6245C35.9387 76.6816 37.4362 75.1272 39.4545 75.1272C41.4728 75.1272 42.9702 76.6816 42.9702 78.5597C42.9702 80.4379 41.4728 81.9923 39.3894 81.9923C37.4362 82.057 35.9387 80.5027 35.9387 78.6245ZM41.4728 78.6245C41.4728 77.4587 40.6264 76.4873 39.4545 76.4873C38.2174 76.4873 37.4362 77.4587 37.4362 78.5597C37.4362 79.7255 38.2825 80.697 39.4545 80.697C40.6915 80.7617 41.4728 79.7903 41.4728 78.6245Z" fill="white"></path><path d="M44.5328 75.3216H46.0302V76.8112C46.4209 75.8397 47.2021 75.1273 48.3741 75.1921V76.8112H48.3089C47.0068 76.8112 46.0302 77.6531 46.0302 79.4018V81.9276H44.5328V75.3216Z" fill="white"></path><path d="M49.7413 75.3215H51.2387V76.293C51.6945 75.7101 52.2153 75.1272 53.257 75.1272C54.2336 75.1272 54.8847 75.5806 55.2102 76.293C55.7311 75.5806 56.4472 75.1272 57.4889 75.1272C58.9213 75.1272 59.8328 76.0339 59.8328 77.653V81.8627H58.3353V78.1064C58.3353 77.0701 57.8145 76.4873 56.9681 76.4873C56.1217 76.4873 55.5357 77.0701 55.5357 78.1064V81.8627H54.0383V78.1064C54.0383 77.0701 53.5174 76.4873 52.6711 76.4873C51.8247 76.4873 51.2387 77.1349 51.2387 78.1711V81.8627H49.7413V75.3215Z" fill="white"></path><path d="M65.7575 81.9276V81.0857C65.3017 81.6038 64.5856 82.0571 63.609 82.0571C62.3719 82.0571 61.2651 81.3447 61.2651 80.0494C61.2651 78.5598 62.4371 77.9122 63.9345 77.9122C64.7158 77.9122 65.2366 78.0417 65.7575 78.1713V78.0417C65.7575 77.135 65.1715 76.6169 64.1298 76.6169C63.4137 76.6169 62.8277 76.8112 62.2417 77.0055L61.8511 75.775C62.5673 75.4511 63.2834 75.2568 64.3902 75.2568C66.3434 75.2568 67.32 76.2931 67.32 78.0417V81.9276H65.7575ZM65.8226 79.1427C65.4319 79.0132 64.9111 78.8837 64.3251 78.8837C63.3485 78.8837 62.8277 79.2723 62.8277 79.9199C62.8277 80.5676 63.4137 80.8914 64.1298 80.8914C65.1064 80.8914 65.8877 80.3085 65.8877 79.5313V79.1427H65.8226Z" fill="white"></path><path d="M72.5285 78.6247C72.5285 76.4227 74.0259 75.1922 75.5885 75.1922C76.6953 75.1922 77.3464 75.7103 77.8672 76.358V72.7959H79.3647V81.9277H77.8672V80.8267C77.3464 81.5392 76.6953 82.0573 75.5885 82.0573C74.0259 82.0573 72.5285 80.8267 72.5285 78.6247ZM77.8672 78.6247C77.8672 77.3294 76.9557 76.4875 75.914 76.4875C74.8723 76.4875 74.0259 77.2647 74.0259 78.6247C74.0259 79.92 74.9374 80.762 75.914 80.762C76.9557 80.762 77.8672 79.92 77.8672 78.6247Z" fill="white"></path><path d="M80.9272 78.6245C80.9272 76.6816 82.2945 75.1272 84.1826 75.1272C86.266 75.1272 87.3728 76.8111 87.3728 78.6893C87.3728 78.8188 87.3728 78.9483 87.3728 79.1426H82.4898C82.6851 80.2436 83.4013 80.8265 84.3779 80.8265C85.0941 80.8265 85.68 80.5674 86.2009 80.0493L87.1124 80.8265C86.4613 81.6037 85.6149 82.057 84.3779 82.057C82.4247 82.057 80.9272 80.697 80.9272 78.6245ZM85.8102 78.1711C85.68 77.1997 85.1592 76.4225 84.1175 76.4225C83.206 76.4225 82.5549 77.1349 82.4247 78.1711H85.8102Z" fill="white"></path><path d="M92.8417 73.1843H99.3524V74.5444H94.3392V76.8112H98.7664V78.1712H94.3392V80.5028H99.3524V81.8628H92.7766V73.1843H92.8417Z" fill="white"></path><path d="M100.589 78.6247C100.589 76.4227 102.087 75.1922 103.649 75.1922C104.756 75.1922 105.407 75.7103 105.928 76.358V72.7959H107.426V81.9277H105.928V80.8267C105.407 81.5392 104.756 82.0573 103.649 82.0573C102.152 82.0573 100.589 80.8267 100.589 78.6247ZM105.993 78.6247C105.993 77.3294 105.082 76.4875 104.04 76.4875C102.998 76.4875 102.152 77.2647 102.152 78.6247C102.152 79.92 103.063 80.762 104.04 80.762C105.082 80.762 105.993 79.92 105.993 78.6247Z" fill="white"></path><path d="M109.314 79.5312V75.3215H110.811V79.0779C110.811 80.1141 111.332 80.697 112.243 80.697C113.155 80.697 113.741 80.1141 113.741 79.0779V75.3215H115.238V81.9275H113.741V80.8913C113.285 81.539 112.699 82.0571 111.657 82.0571C110.16 82.0571 109.314 81.0208 109.314 79.5312Z" fill="white"></path><path d="M116.801 78.6245C116.801 76.7463 118.233 75.1272 120.251 75.1272C121.489 75.1272 122.27 75.5806 122.921 76.293L122.009 77.3292C121.554 76.8111 121.033 76.4873 120.317 76.4873C119.21 76.4873 118.429 77.4587 118.429 78.5597C118.429 79.7255 119.275 80.697 120.447 80.697C121.163 80.697 121.684 80.3732 122.14 79.855L123.051 80.7617C122.4 81.4742 121.619 81.9923 120.317 81.9923C118.298 82.057 116.801 80.5027 116.801 78.6245Z" fill="white"></path><path d="M128.39 81.9276V81.0857C127.934 81.6038 127.218 82.0571 126.241 82.0571C125.004 82.0571 123.897 81.3447 123.897 80.0494C123.897 78.5598 125.069 77.9122 126.567 77.9122C127.348 77.9122 127.869 78.0417 128.39 78.1713V78.0417C128.39 77.135 127.804 76.6169 126.762 76.6169C126.046 76.6169 125.46 76.8112 124.874 77.0055L124.483 75.775C125.2 75.4511 125.916 75.2568 127.023 75.2568C128.976 75.2568 129.952 76.2931 129.952 78.0417V81.9276H128.39ZM128.455 79.1427C128.064 79.0132 127.543 78.8837 126.957 78.8837C125.981 78.8837 125.46 79.2723 125.46 79.9199C125.46 80.5676 126.046 80.8914 126.762 80.8914C127.739 80.8914 128.52 80.3085 128.52 79.5313V79.1427H128.455Z" fill="white"></path><path d="M132.947 83.4173L133.924 81.9277C132.426 81.5391 131.385 80.2438 131.385 78.6247C131.385 76.7465 132.817 75.1921 134.835 75.1921C136.072 75.1921 136.854 75.6455 137.505 76.3579L136.593 77.3941C136.137 76.876 135.617 76.5522 134.9 76.5522C133.794 76.5522 133.012 77.5237 133.012 78.6247C133.012 79.7904 133.859 80.7619 135.031 80.7619C135.747 80.7619 136.268 80.4381 136.723 79.92L137.635 80.8267C137.049 81.4743 136.333 81.9924 135.226 82.0572L134.38 84.0002L132.947 83.4173Z" fill="white"></path><path d="M142.974 81.9276V81.0857C142.518 81.6038 141.802 82.0572 140.825 82.0572C139.588 82.0572 138.481 81.3448 138.481 80.0495C138.481 78.5599 139.653 77.9122 141.151 77.9122C141.932 77.9122 142.453 78.0417 142.974 78.1713V78.0417C142.974 77.135 142.388 76.6169 141.346 76.6169C140.63 76.6169 140.044 76.8112 139.458 77.0055L139.067 75.775C139.783 75.4512 140.5 75.2569 141.606 75.2569C143.56 75.2569 144.536 76.2931 144.536 78.0417V81.9276H142.974ZM139.328 74.3502C139.588 73.3787 139.979 72.7958 140.76 72.7958C141.476 72.7958 142.127 73.3139 142.583 73.3139C142.974 73.3139 143.104 73.1196 143.299 72.6663L144.146 72.9253C143.885 73.9616 143.494 74.4797 142.713 74.4797C141.997 74.4797 141.346 73.9616 140.89 73.9616C140.5 73.9616 140.369 74.1559 140.174 74.6092L139.328 74.3502ZM143.039 79.1427C142.648 79.0132 142.127 78.8837 141.541 78.8837C140.565 78.8837 140.044 79.2723 140.044 79.9199C140.044 80.5676 140.63 80.8914 141.346 80.8914C142.323 80.8914 143.104 80.3085 143.104 79.5313V79.1427H143.039Z" fill="white"></path><path d="M145.969 78.6245C145.969 76.6816 147.466 75.1272 149.484 75.1272C151.503 75.1272 153 76.6816 153 78.5597C153 80.4379 151.503 81.9923 149.419 81.9923C147.466 82.057 145.969 80.5027 145.969 78.6245ZM151.503 78.6245C151.503 77.4587 150.656 76.4873 149.484 76.4873C148.247 76.4873 147.466 77.4587 147.466 78.5597C147.466 79.7255 148.312 80.697 149.484 80.697C150.721 80.7617 151.503 79.7903 151.503 78.6245Z" fill="white"></path><path d="M140.89 33.7425C134.184 27.0717 126.502 27.3308 119.34 22.0848C116.671 20.1419 115.564 17.5513 115.564 15.2197C115.564 9.90902 120.968 5.50501 127.478 5.50501C133.533 5.50501 138.091 8.87278 139.197 13.212L141.802 6.15266C138.026 3.75636 133.208 2.26677 127.999 2.26677C120.186 2.26677 113.285 5.50501 109.379 10.4271C107.295 13.0825 106.123 16.256 106.123 19.6237C106.123 22.7325 107.035 25.6469 108.858 28.1727C113.676 34.9083 122.14 36.0093 128.976 39.7656C132.361 41.6438 135.421 44.3639 135.421 48.5089C135.421 54.1434 129.562 58.7417 122.074 58.7417C115.043 58.7417 109.509 54.532 108.793 49.286L105.407 58.3531C109.574 60.6847 115.369 62.0447 121.098 62.0447C129.497 62.0447 136.398 59.0008 140.89 54.3377C143.69 51.4233 145.383 47.8612 145.383 43.9753C145.448 40.2837 143.82 36.6569 140.89 33.7425ZM77.0209 0C58.4005 0 42.84 13.2768 39.3894 30.8928C33.5949 27.0069 27.3447 26.4888 21.3549 22.1496C18.6856 20.2066 17.5788 17.616 17.5788 15.2845C17.5788 9.97379 22.9826 5.56978 29.4932 5.56978C35.5481 5.56978 40.1056 8.93755 41.2124 13.2768L43.8166 6.21743C40.0405 3.82113 35.2226 2.33153 30.0141 2.33153C22.2013 2.33153 15.3 5.56978 11.3936 10.4919C9.31024 13.1473 8.13833 16.3207 8.13833 19.6885C8.13833 22.7972 9.04982 25.7116 10.8728 28.2375C15.8209 34.973 24.2847 36.1388 31.1209 39.8951C34.5064 41.7733 37.5664 44.4934 37.5664 48.6384C37.5664 54.2729 31.7068 58.8712 24.2196 58.8712C17.1881 58.8712 11.6541 54.6615 10.9379 49.4156L7.55237 58.4827C11.7192 60.8142 17.5136 62.1743 23.243 62.1743C31.6417 62.1743 38.543 59.1303 43.0353 54.4672C45.8349 51.5528 47.5277 47.9907 47.5277 44.1049C47.5277 40.219 45.9 36.5921 42.9702 33.6777C42.9051 33.5482 42.7749 33.4834 42.7098 33.4187C45.7698 18.3285 57.6843 6.47648 72.8541 3.4973L71.4868 6.67078L56.1868 42.8096L54.429 46.9545L48.5694 60.7494H53.5826L59.1817 47.6022C59.3771 47.2136 59.7026 46.9545 60.1583 46.9545H83.1409C83.5966 46.9545 83.9873 47.2783 84.1175 47.6669L88.1541 60.6847H101.24L97.0085 46.8897L95.7064 42.7448L83.4013 2.97918C91.2792 3.62683 98.4409 6.60601 104.3 11.2691C97.3341 4.33924 87.6983 0 77.0209 0ZM81.1877 42.8096H62.7626C61.9813 42.8096 61.5256 42.0324 61.786 41.32L72.3983 16.256C72.789 15.3493 74.0911 15.414 74.4166 16.3855L82.1643 41.4495C82.3596 42.0971 81.8388 42.8096 81.1877 42.8096Z" fill="white"></path>
                                        </svg>
                                    </td>
                                    <td class="focos">
                                        <svg id="logo-focos" data-name="logo-focos" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 330.41 84">
                                            <defs><style>.cls-1{fill:#192e7c;}.cls-2{fill:#fc492a;}.cls-3{fill:#cd2510;}</style></defs>
                                            <path class="cls-1" d="M99,11.83h38v13h-22.8v5.77h21.18V42.34H114.11V57.18H99Z" transform="translate(0 0)"/><path class="cls-1" d="M139.44,34.63V34.5c0-13.15,10.81-23.65,24.81-23.65s24.69,10.37,24.69,23.53v.13c0,13.15-10.82,23.64-24.82,23.64S139.44,47.78,139.44,34.63Zm34.2,0V34.5c0-5.38-3.69-10.23-9.52-10.23s-9.4,4.79-9.4,10.1v.13c0,5.38,3.7,10.24,9.53,10.24S173.64,39.94,173.64,34.63Z" transform="translate(0 0)"/><path class="cls-1" d="M192,34.63V34.5c0-13.54,10.56-23.65,24.1-23.65,10,0,16.71,4.87,20.53,11.8L224.17,30c-1.68-3.24-4.08-5.51-8.23-5.51-5.12,0-8.48,4.47-8.48,9.91v.13c0,5.9,3.49,10,8.48,10,4.28,0,6.68-2.33,8.56-5.7L237,45.9c-3.82,6.74-10.17,12.25-21.45,12.25C203,58.15,192,48.69,192,34.63Z" transform="translate(0 0)"/><path class="cls-1" d="M237.58,34.63V34.5c0-13.15,10.82-23.65,24.81-23.65s24.69,10.37,24.69,23.53v.13c0,13.15-10.82,23.64-24.81,23.64S237.58,47.78,237.58,34.63Zm34.21,0V34.5c0-5.38-3.7-10.23-9.53-10.23s-9.39,4.79-9.39,10.1v.13c0,5.38,3.69,10.24,9.52,10.24S271.79,39.94,271.79,34.63Z" transform="translate(0 0)"/><path class="cls-1" d="M287.14,50.38l8.1-9.65a26,26,0,0,0,15.87,5.32c2.72,0,3.89-.72,3.89-1.95V44c0-1.29-1.43-2-6.29-3-10.17-2.07-19.11-5-19.11-14.57v-.13c0-8.62,6.74-15.29,19.24-15.29,8.74,0,15.22,2.07,20.47,6.22l-7.38,10.24A23.84,23.84,0,0,0,308.32,23c-2.27,0-3.3.78-3.3,1.88V25c0,1.23,1.23,2,6,2.92,11.6,2.13,19.37,5.5,19.37,14.64v.13c0,9.52-7.84,15.35-20,15.35C301.2,58,292.91,55.43,287.14,50.38Z" transform="translate(0 0)"/><path class="cls-1" d="M99.23,67.29h7.56v1.63H101v2.77h5.12v1.63H101v4.14H99.23Z" transform="translate(0 0)"/><path class="cls-1" d="M108.22,72.4v0a5.2,5.2,0,0,1,5.15-5.26h.17a5.16,5.16,0,0,1,5.3,5.23v0a5.21,5.21,0,0,1-5.16,5.25h-.18A5.14,5.14,0,0,1,108.22,72.4Zm8.75,0v0a3.49,3.49,0,0,0-3.46-3.62,3.44,3.44,0,0,0-3.42,3.59v0A3.48,3.48,0,0,0,113.54,76,3.43,3.43,0,0,0,117,72.4Z" transform="translate(0 0)"/><path class="cls-1" d="M121,67.29h4.53a4.09,4.09,0,0,1,2.93,1,3.06,3.06,0,0,1,.84,2.19v0a3.05,3.05,0,0,1-2.42,3.08l2.74,3.85h-2.1L125,73.91h-2.24v3.54H121Zm4.4,5c1.27,0,2.09-.67,2.09-1.7v0c0-1.09-.78-1.68-2.09-1.68h-2.62v3.41Z" transform="translate(0 0)"/><path class="cls-1" d="M131.49,67.29h1.9l3.09,4.8,3.1-4.8h1.9V77.45h-1.79V70.16L136.48,75h0l-3.18-4.77v7.26h-1.76Z" transform="translate(0 0)"/><path class="cls-1" d="M147.69,67.22h1.65l4.47,10.23h-1.88l-1-2.46h-4.81l-1,2.46h-1.83Zm2.56,6.19-1.76-4.06-1.74,4.06Z" transform="translate(0 0)"/><path class="cls-1" d="M157.47,79.2l1.13-1.68a5.09,5.09,0,0,1-4.12-5.12v0a5.14,5.14,0,0,1,5.24-5.25,5.31,5.31,0,0,1,4,1.58L162.55,70a4.09,4.09,0,0,0-2.85-1.26,3.41,3.41,0,0,0-3.35,3.59v0A3.43,3.43,0,0,0,159.7,76a4,4,0,0,0,2.92-1.32l1.15,1.16A5.2,5.2,0,0,1,160,77.61l-1,2.25Z" transform="translate(0 0)"/><path class="cls-1" d="M169.24,67.22h1.65l4.47,10.23h-1.89l-1-2.46h-4.8l-1,2.46h-1.84Zm-1.89-1.08c.31-1.13.74-1.78,1.63-1.78s1.63.57,2.13.57.6-.24.8-.7l1,.29c-.32,1.13-.76,1.78-1.63,1.78s-1.63-.56-2.14-.56-.59.23-.81.71Zm4.44,7.27L170,69.35l-1.75,4.06Z" transform="translate(0 0)"/><path class="cls-1" d="M176,72.4v0a5.2,5.2,0,0,1,5.15-5.26h.17a5.16,5.16,0,0,1,5.3,5.23v0a5.2,5.2,0,0,1-5.15,5.25h-.17A5.15,5.15,0,0,1,176,72.4Zm8.75,0v0a3.48,3.48,0,0,0-3.45-3.62,3.44,3.44,0,0,0-3.43,3.59v0A3.48,3.48,0,0,0,181.35,76a3.43,3.43,0,0,0,3.42-3.58Z" transform="translate(0 0)"/><path class="cls-1" d="M192.72,72.4v0A5.14,5.14,0,0,1,198,67.11a5.3,5.3,0,0,1,4,1.59L200.78,70a4,4,0,0,0-2.84-1.27,3.41,3.41,0,0,0-3.35,3.59v0A3.43,3.43,0,0,0,197.94,76a4,4,0,0,0,2.92-1.32L202,75.83a5.24,5.24,0,0,1-4.12,1.79A5.1,5.1,0,0,1,192.72,72.4Z" transform="translate(0 0)"/><path class="cls-1" d="M203.13,72.4v0a5.22,5.22,0,0,1,5.16-5.26h.17a5.16,5.16,0,0,1,5.3,5.23v0a5.21,5.21,0,0,1-5.16,5.25h-.17A5.16,5.16,0,0,1,203.13,72.4Zm8.76,0v0a3.49,3.49,0,0,0-3.46-3.62A3.43,3.43,0,0,0,205,72.34v0A3.47,3.47,0,0,0,208.46,76a3.43,3.43,0,0,0,3.43-3.58Z" transform="translate(0 0)"/><path class="cls-1" d="M215.92,67.29h1.66l5.44,7v-7h1.75V77.45h-1.49l-5.59-7.22v7.22h-1.76Z" transform="translate(0 0)"/><path class="cls-1" d="M229.88,68.94h-3.22V67.29h8.24v1.65h-3.22v8.51h-1.8Z" transform="translate(0 0)"/><path class="cls-1" d="M236.89,67.29h1.79V77.45h-1.79Z" transform="translate(0 0)"/><path class="cls-1" d="M241.39,67.29H243l5.44,7v-7h1.76V77.45h-1.5l-5.6-7.22v7.22h-1.75Z" transform="translate(0 0)"/><path class="cls-1" d="M252.69,73.14V67.29h1.79v5.77c0,1.89,1,2.91,2.56,2.91s2.56-1,2.56-2.83V67.29h1.79v5.76c0,3-1.71,4.56-4.37,4.56S252.69,76.08,252.69,73.14Z" transform="translate(0 0)"/><path class="cls-1" d="M267.07,67.22h1.66l4.47,10.23h-1.89l-1-2.46h-4.8l-1.05,2.46H262.6Zm2.57,6.19-1.76-4.06-1.74,4.06Z" transform="translate(0 0)"/><path class="cls-1" d="M274.94,67.29h3.79a5.06,5.06,0,0,1,5.39,5v0a5.08,5.08,0,0,1-5.39,5.08h-3.79Zm3.79,8.53a3.31,3.31,0,0,0,3.52-3.42v0a3.33,3.33,0,0,0-3.52-3.45h-2v6.9Z" transform="translate(0 0)"/><path class="cls-1" d="M289.19,67.22h1.65l4.47,10.23h-1.88l-1-2.46h-4.8l-1,2.46h-1.83Zm2.56,6.19L290,69.35l-1.74,4.06Z" transform="translate(0 0)"/><path class="cls-1" d="M300.73,76l1.07-1.28A4.67,4.67,0,0,0,305,76c1.1,0,1.79-.51,1.79-1.28v0c0-.72-.4-1.11-2.29-1.55-2.16-.52-3.38-1.16-3.38-3v0c0-1.74,1.45-2.94,3.47-2.94a5.64,5.64,0,0,1,3.69,1.28l-1,1.34a4.64,4.64,0,0,0-2.76-1c-1,0-1.65.54-1.65,1.21h0c0,.78.46,1.13,2.41,1.59,2.14.53,3.26,1.3,3.26,3v0c0,1.9-1.5,3-3.63,3A6.25,6.25,0,0,1,300.73,76Z" transform="translate(0 0)"/><path class="cls-1" d="M314.08,67.22h1.65l4.47,10.23h-1.89l-1-2.46h-4.8l-1.05,2.46h-1.81Zm2.56,6.19-1.76-4.06-1.74,4.06Z" transform="translate(0 0)"/><path class="cls-1" d="M321.08,76l1.08-1.28A4.67,4.67,0,0,0,325.37,76c1.1,0,1.8-.51,1.8-1.28v0c0-.72-.41-1.11-2.3-1.55-2.16-.52-3.38-1.16-3.38-3v0c0-1.74,1.45-2.94,3.47-2.94a5.6,5.6,0,0,1,3.68,1.28l-.95,1.34a4.64,4.64,0,0,0-2.76-1c-1,0-1.65.54-1.65,1.21h0c0,.78.46,1.13,2.4,1.59,2.15.53,3.27,1.3,3.27,3v0c0,1.9-1.5,3-3.63,3A6.31,6.31,0,0,1,321.08,76Z" transform="translate(0 0)"/><path class="cls-2" d="M42,18.75a23.25,23.25,0,0,1,0,46.5,7.71,7.71,0,1,1-.27-15.42H42a7.83,7.83,0,0,0,0-15.66,7.71,7.71,0,1,1-.27-15.42Z" transform="translate(0 0)"/><path class="cls-3" d="M41.83,0a7.72,7.72,0,0,0,0,15.43,26.57,26.57,0,1,1,0,53.14h0a7.72,7.72,0,0,0,0,15.43,42,42,0,0,0,0-84Z" transform="translate(0 0)"/><path class="cls-2" d="M42.23,65.25a23.25,23.25,0,0,1,0-46.5,7.71,7.71,0,1,1,.27,15.42h-.27a7.83,7.83,0,0,0,0,15.66,7.71,7.71,0,1,1,0,15.42Z" transform="translate(0 0)"/><path class="cls-3" d="M42,84A42,42,0,0,1,42,0a7.72,7.72,0,1,1,.27,15.43H42a26.57,26.57,0,0,0,0,53.14A7.72,7.72,0,1,1,42.27,84Z" transform="translate(0 0)"/>
                                        </svg> 
                                    </td>
                                </tr>
                            </table> 
                        </div>
                    </div>
                </div>
            </div>            
        </footer>
    </body>
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="./assets/slick/slick.js"></script>
    <script src="js/jquery.colorbox.js"></script>
    <script src="js/jquery.mask.min.js"></script>
    <script src="js/loadingoverlay.min.js"></script>
    <script src="js/loadingoverlay_progress.js"></script>
    <script src="js/select2.full.min.js"></script>
    <script src="js/main.js"></script>
    <script type="text/javascript">
        $(document).ready(function() { 
            $("#cidade").select2(); 
            $("#escola").select2(); 
        });
    </script>

    <script>
        const pasteBox = document.getElementById("email2");
        pasteBox.onpaste = e => {
            e.preventDefault();
            return false;
        };

        function checkEmailConfirmation() {
            let email = document.getElementById("email").value;
            let emailConfirmation = document.getElementById("email2").value;
            if(email != emailConfirmation) {
                document.getElementById("mensagem-email-diferente").style.display="block"
            } else {
                document.getElementById("mensagem-email-diferente").style.display="none"
            }
        }
    </script>
</html>