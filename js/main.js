$('.carrossel-mensagem').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 6000,
    speed: 1500,
});

$(document).on('click', 'header h6', function (event) {
    var target_offset = $("#preparese").offset();
    var target_top = target_offset.top;
    $('html, body').animate({scrollTop: target_top}, 1500);
});

$('#telefone').mask("(99) 9999-99999");
    
    $('#telefone').keyup(function () {
        var phone, element;
        element = $(this);
        element.unmask();
        phone = element.val().replace(/\D/g, '');
        if (phone.length > 10) {
            element.mask("(99) 9 9999-9999");
        } else {
            element.mask("(99) 9999-99999");
        }
    }).trigger('focusout');

$("#cep").mask("00.000-000");

$('#cep').on('change', function () {
    var cep = $(this).val().replace(/[^\d]+/g, '')
    $.ajax({
        url: "https://viacep.com.br/ws/" + cep + "/json/",
        success: function (result) {					
            $('#estado option[value=' + result.uf + ']').attr('selected','selected');					
            $.ajax({
                type: "post",
                url: "js/ajax/cidades.php",
                data: 'estado=' + result.uf + '&id=' + result.ibge,
                dataType: "html",
                success: function (response) {
                    $('#cidade').html(response);							
                    $.ajax({
                        type: "post",
                        url: "js/ajax/escolas.php",
                        data: 'cidade=' + result.ibge,
                        dataType: "html",
                        success: function (response) {
                                $('#escola').html(response);
                        }
                    });							
                }
            });
        }
    });
});

$(document).on('change', '#estado', function (event) {
    event.preventDefault();
    var estado = $('option:selected', this).val();
    $.ajax({
        type: "post",
        url: "js/ajax/cidades.php",
        data: 'estado=' + estado,
        dataType: "html",
        success: function (response) {
            $('#cidade').html(response);
        }
    });
});

$(document).on('change', '#cidade', function (event) {
    event.preventDefault();
    var cidade = $('option:selected', this).val();
    $.ajax({
        type: "post",
        url: "js/ajax/escolas.php",
        data: 'cidade=' + cidade,
        dataType: "html",
        success: function (response) {
            $('#escola').html(response);
        }
    });
});

$('#local_outra_escola').hide();

$(document).on('click','.ver-senha', function (e) {
    if ($(this).hasClass('fa-eye')) {
        $(this).removeClass('fa-eye')
        $(this).addClass('fa-eye-slash')
        $(this).siblings('#senha').attr('type', 'text');
        $(this).siblings('#senha').focus();
    } else {
        $(this).removeClass('fa-eye-slash')
        $(this).addClass('fa-eye')
        $(this).siblings('#senha').attr('type', 'password');
        $(this).siblings('#senha').focus();
    }     
});     

var $brandImage = $("#brand-image");
var $scrollHelper = $("#scroll-helper");
var count_scroll = 0;

$(window).scroll(function() {
    var parada = $("#preparese").offset().top;
    if (this.pageYOffset >= (parada -10)) {
        $('header .inscrever, header .cadastrado').fadeOut('slow')
    } else {
        $('header .inscrever, header .cadastrado').fadeIn('slow')
    }
});

$(document).on('submit','form#inscricao', function (event) {
    var form = $(this);
    var txtBtn = $('button[type="submit"]',form).html();
    var action = $(this).attr('action');
    event.preventDefault();
    $.ajax({
        type: "POST",
        url: 'js/ajax/'+action,
        data: $(this).serialize(),
        dataType: "JSON",
        beforeSend: function() {
            $('button[type="submit"]',form).attr('disabled',true);
            $('.loading').css('display', 'block');
        },
        success: function (response) {
            if (response.status == 'Ok') {
                $('#modal-resposta .modal-body .mensagem').html(response.mensagem);
                $('#modal-resposta').modal();
                $('form#inscricao select option').removeAttr('selected');
                $('form#inscricao select option[value=""]').attr('selected','selected');	
                $('form#inscricao').each(function(){
                    this.reset();                            
                });
            } else {
                $('#modal-resposta .modal-body .mensagem').html(response.mensagem);
                $('#modal-resposta').modal();
            }
        },
        complete: function() {
            $('button[type="submit"]',form).attr('disabled',false);
            $('.loading').css('display', 'none');
        }
    });
});
        
$(window).on("load", function () {
    $.LoadingOverlay("hide");
    $('body').removeClass('unloaded');
    $('body').addClass('loaded');
    
    $(function() {
        setTimeout(function(){ 
            $(".loading").fadeOut();
        }, 10);                    
    }), jQuery(document).on("click", ".mega-dropdown", function(e) {
        e.stopPropagation()
    });
    
    $(".animated.unload").each(function () {
        var $this = $(this),
                $animation = ($this.data("animation") !== undefined) ? $this.data("animation") : "slideUp";
        $delay = ($this.data("delay") !== undefined) ? $this.data("delay") : 300;
        setTimeout(function () {
            $this.addClass($animation);
            $this.addClass('inview');
        }, $delay);
    });
});

$(".modal").on('shown.bs.modal', function () {
    $('header h6').fadeOut('slow');    
});

$(".modal").on('hidden.bs.modal', function () {
    $('header h6').fadeIn('slow');
});