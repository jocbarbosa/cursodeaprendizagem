<?php
	class User extends Geral{
		
		static function AddUser($nome,$email,$login,$senha,$id=0){
			$senhacrip = sha1($senha);
			if($id == 0) {
				$hoje = date('Y-m-d');
				$insert = self::getConn()->prepare('INSERT INTO `user` SET `nome`=?, `email`=?, `login`=?, `senha`=?, `data`=?');
				$insert->execute(array($nome,$email,$login,$senhacrip,$hoje));
			}
			else {
				if($senha != '') {
					$insert = self::getConn()->prepare('UPDATE `user` SET `senha`=? WHERE `id`=?');
					$insert->execute(array($senhacrip,$id));
				}
				$insert = self::getConn()->prepare('UPDATE `user` SET `nome`=?, `email`=?, `login`=? WHERE `id`=?');
				$insert->execute(array($nome,$email,$login,$id));
			}
			$lastId = $id;
			return $lastId;
		}

		static function SelectUser($id=0){
			if($id == 0) {
				$list = self::getConn()->prepare('SELECT * FROM `user`');
				$list->execute(array());
			}
			else {
				$list = self::getConn()->prepare('SELECT * FROM `user` WHERE `id`=?');
				$list->execute(array($id));
			}
			$d['num'] = $list->rowCount();
			$d['dados'] = $list->fetchAll();
			return $d;
		}

		static function DeleteUser($id){
			$del = self::getConn()->prepare('DELETE FROM `user` WHERE `id`=?');
			$del->execute(array($id));
			return $del;
		}

	}
?>