<?php include("../../classes/config.php"); ?>

<?php
    extract($_POST);
    
    if ($pais == 'BR') {
        $estados = $estadosBrasileiros;
    } else if ($pais == 'JP') {
        $estados = $estadosJaponeses;
    }
    
    echo '<option value=""></option>';
    foreach ($estados as $sigla => $estado) {
        echo '<option value="' . $sigla . '">' . $estado . '</option>';
    }
?>